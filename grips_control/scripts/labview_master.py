#! /usr/bin/env python
import rospy, time
from math import pi
from cStringIO import StringIO
# Sockets
import socket, struct
# Messages
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState
from control_msgs.msg import JointControllerState

class LabviewMaster:
	SA = 0; SE = 1; EL = 2; WP = 3; WY = 4; WR = 5; GRIP = 6
	joint_names = ['SA', 'SE', 'linkage_tr', 'WP', 'WY', 'WR', 'GRIP']
	def __init__(self):	
		self.ns = rospy.get_namespace()
		# Set up receiver socket
		self.read_port = 5555
		self.read_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.read_socket.bind(('', self.read_port))
		rospy.loginfo('UDP Socket receiving on port [%d]' % (self.read_port))
		# Set up sender socket
		self.write_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.labview_ip = '138.100.76.191'
		self.write_port = 5050		
		rospy.loginfo('UDP Socket sending to [udp://%s:%d]' % (self.labview_ip, self.write_port))
		#Set-up publishers/subscribers
		self.pub_command = []
		self.commands = []
		for name in self.joint_names:
			self.pub_command.append(rospy.Publisher('%s%s/command' % (self.ns, name), Float64))
			self.commands.append(0)         
		
		self.pos_error = [0, 0, 0, 0, 0, 0]
		rospy.Subscriber('%s%s/state' % (self.ns, self.joint_names[0]), JointControllerState, self.cb_SA)
		rospy.Subscriber('%s%s/state' % (self.ns, self.joint_names[1]), JointControllerState, self.cb_SE)
		rospy.Subscriber('%s%s/state' % (self.ns, self.joint_names[2]), JointControllerState, self.cb_linkage_tr)
		rospy.Subscriber('%s%s/state' % (self.ns, self.joint_names[3]), JointControllerState, self.cb_WP)
		rospy.Subscriber('%s%s/state' % (self.ns, self.joint_names[4]), JointControllerState, self.cb_WY)
		rospy.Subscriber('%s%s/state' % (self.ns, self.joint_names[5]), JointControllerState, self.cb_WR)
		rospy.Subscriber('%sjoint_states' % self.ns, JointState, self.cb_joint_states)
		while not rospy.is_shutdown():
			msg = JointState()
			data = self.recv_timeout()
			if data:
				msg.deserialize(data)
				self.commands = self.adjust_position(msg.position)
				for i, angle in enumerate(self.commands):
					self.pub_command[i].publish(angle)	
	
	def cb_SA(self, msg):
		self.pos_error[0] = msg.error
	
	def cb_SE(self, msg):
		self.pos_error[1] = msg.error
		
	def cb_linkage_tr(self, msg):
		self.pos_error[2] = msg.error
		
	def cb_WP(self, msg):
		self.pos_error[3] = msg.error
		
	def cb_WY(self, msg):
		self.pos_error[4] = msg.error
		
	def cb_WR(self, msg):
		self.pos_error[5] = msg.error
	
	def cb_joint_states(self, msg):
		state = JointState()
		for i in xrange(len(self.joint_names)):
			for j in xrange(len(msg.name)):
				if self.joint_names[i] == msg.name[j]:
					state.name.append(msg.name[j])
					state.position.append(msg.position[j])
					state.velocity.append(msg.velocity[j])
					#~ pos_error = msg.position[j] - self.commands[i]
					#~ state.effort.append(pos_error - msg.velocity[j])
					break
		state.effort = self.pos_error
		file_str = StringIO()
		state.serialize(file_str)
		self.write_socket.sendto(file_str.getvalue(), (self.labview_ip, self.write_port))
	
	def adjust_position(self, values):
		position = list(values)
		position[self.SA] = -position[self.SA]
		position[self.EL] = pi - position[self.EL] - position[self.SE]
		position[self.WP] = -position[self.WP]
		position[self.WY] = -position[self.WY]
		return position
		
	def recv_timeout(self, timeout=0.001):
		self.read_socket.setblocking(0)
		total_data=[]
		data=''
		begin=time.time()
		while 1:
			#if you got some data, then timeout break 
			if total_data and time.time()-begin>timeout:
				break
			#if you got no data at all, wait a little longer
			elif time.time()-begin>timeout*2:
				break
			try:
				data=self.read_socket.recv(8192)
				if data:
					total_data.append(data)
					begin=time.time()
			except:
				pass
		return ''.join(total_data)
		
if __name__ == '__main__':
	rospy.init_node('labview_master')
	master = LabviewMaster()
	
