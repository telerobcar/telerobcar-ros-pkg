#!/usr/bin/env python
import roslib; roslib.load_manifest('robot_manager')
import rospy

import sys
import signal

#libraries to talk to postgresql
import psycopg2
import ppygis

#libraries to read openrave database
import h5py

import progressbar as pbar

connection = []
cursor = []
    
def create_robot(robot_name):
    global connection, cursor
    
    rospy.loginfo("Creating Tables 'robots', 'reachability_points' & 'reachability_orientations' if they don't exist already.")
    query = """CREATE TABLE IF NOT EXISTS robots (
		    robot_id SERIAL PRIMARY KEY,
		    robot_name VARCHAR (50) UNIQUE NOT NULL,
		    created_on TIMESTAMP NOT NULL DEFAULT current_timestamp)
		    """	
    cursor.execute(query)
    query = """CREATE TABLE IF NOT EXISTS reachability_points (
		    point_id SERIAL PRIMARY KEY,
		    robot_id INTEGER,
		    point GEOMETRY)
		    """	
    cursor.execute(query)
    query = """CREATE TABLE IF NOT EXISTS reachability_orientations (
		    robot_id INTEGER,
		    point_id INTEGER,
		    orientation GEOMETRY)
		    """	
    cursor.execute(query)
    connection.commit()
    
    # check that the robot doesn't already exist
    query = """SELECT exists(
		    SELECT robot_name 
		    FROM robots 
		    WHERE robot_name = %s)"""
    cursor.execute(query,(robot_name,))
    robot_exists = cursor.fetchone()[0]
    if (robot_exists):
	 if raw_input("\nRobot '" + robot_name + "' Exists. Overwrite? (y/n)> ").lower().startswith('y'):
	     #Delete the previous data from tables
	     query = """SELECT robot_id
			    FROM robots 
			    WHERE robot_name = %s"""
	     cursor.execute(query,(robot_name,))
	     stored_id = cursor.fetchone()
	     query = """DELETE FROM robots 
			    WHERE robot_id = %s"""
	     cursor.execute(query,(stored_id,))
	     query = """DELETE FROM reachability_points 
			    WHERE robot_id = %s"""
	     cursor.execute(query,(stored_id,))
	     query = """DELETE FROM reachability_orientations
			    WHERE robot_id = %s"""
	     cursor.execute(query,(stored_id,))
	     connection.commit()
	 else:
	    print "Robot Load Aborted. Exiting...."
	    sys.exit()
	     
    # Create the new robot entry
    query = """INSERT INTO robots(robot_name) 
		VALUES (%s)"""
    cursor.execute(query,(robot_name,))
    connection.commit()
    
    # Get the ID number assigned by the database
    query = """SELECT robot_id
		FROM robots 
		WHERE robot_name = %s"""
    cursor.execute(query,(robot_name,))
    robot_id = cursor.fetchone()
     
    rospy.loginfo("Loading .pp file from openrave")
    #load the openrave kinematic database
    #find this by using the --getfilename option of the function "openrave.py --database kinematicreachability"
    #~ file_name = '/home/disam/.openrave/robot.f4025675e127122e084d959288e4555d/reachability.27d697e7d8a999dfc3b0a3305edb1ee6.pp'
    file_name = '/home/disam/.openrave/robot.3867dbe8c78c135dca976f550c6d96dc/reachability.3af4aff535c49b2f7572edd391a18d91.pp'
    ppfile = h5py.File(file_name,'r')
    
    #extract the x,y,z points from reachability
    points = []
    orientations = []
    npoints = len(ppfile['reachabilitystats'])
    
    rospy.loginfo("Please wait. Loading " + str(npoints) + " points")
    
    #create progress bar
    widgets = [pbar.Percentage(), pbar.Bar(),' ', pbar.ETA(),' ', pbar.FileTransferSpeed(), pbar.RotatingMarker()]
    progress = pbar.ProgressBar(widgets=widgets, maxval=npoints)
    
    #Make a list of tuples, to ensure no duplicate points go into database
    unique_points = []
    unique_ids = []
    for i in progress(range(npoints)):
	row = ppfile['reachabilitystats'][i,:]
	pt = (row[4],row[5],row[6])
	if pt in unique_points:
	    point_id = unique_ids[unique_points.index(pt)]
	else:
	    query = """INSERT INTO reachability_points(robot_id, point) 
			VALUES (%s,'POINT(%s %s %s)') RETURNING point_id"""
	    cursor.execute(query,(robot_id,pt[0],pt[1],pt[2]))
	    point_id = cursor.fetchone()[0]
	    unique_points.append(pt)
	    unique_ids.append(point_id)
	query = """INSERT INTO reachability_orientations(robot_id, point_id, orientation) 
		VALUES (%s, %s, 'POINT(%s %s %s %s)')"""
	cursor.execute(query,(robot_id,point_id,row[0],row[1],row[2],row[3]))
    connection.commit()
    progress.finish()
    rospy.loginfo("Creating Spatial Indexes")
    progress2 = pbar.ProgressBar(widgets=widgets, maxval=100).start()
    #Create a spatial index (deleting any previous one)
    query = """DROP INDEX reachability_points_point_gist"""
    cursor.execute(query)
    query = """CREATE INDEX reachability_points_point_gist
		ON reachability_points
		USING GIST(point gist_geometry_ops_nd)"""
    cursor.execute(query)
    progress2.update(50)
    query = """DROP INDEX reachability_orientations_orientation_gist"""
    cursor.execute(query)
    query = """CREATE INDEX reachability_orientations_orientation_gist
		ON reachability_orientations
		USING GIST(orientation gist_geometry_ops_nd)"""
    cursor.execute(query)
    connection.commit()
    progress2.finish()
    
def workspace_manager(robot_name):
    global connection, cursor
    #connect to an existing database
    rospy.loginfo('Connecting to database \'robots\'')
    try:
	connection = psycopg2.connect(database = 'testdatabase', user = 'disam')
    except:
	rospy.logerr("ERROR: Could not connect to database 'robots' as user 'disam'")
    cursor = connection.cursor()
    
    rospy.loginfo("Creating Robot " + str(robot_name))
    create_robot(robot_name)
    
    #disconnect from database
    cursor.close()
    connection.close()
    
#this is required, or it will not exit within the 
def exit_gracefully(signum, frame):
    # restore the original signal handler as otherwise evil things will happen
    # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
    signal.signal(signal.SIGINT, original_sigint)

    try:
        if raw_input("\nReally quit? (y/n)> ").lower().startswith('y'):
            sys.exit(1)

    except KeyboardInterrupt:
        print("Ok ok, quitting")
        sys.exit(1)

    # restore the exit gracefully handler here       
    
if __name__ == '__main__':
    # store the original SIGINT handler
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)
    rospy.init_node('workspace_manager')
    workspace_manager(sys.argv[1])	
