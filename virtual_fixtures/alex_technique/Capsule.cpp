#include "alex_technique/Capsule.h"
#include <iostream> //debug only

/**
 * @brief Constructor
 * @param start Point which specifies start of the line.
 * @param end Point which specifies end of the line.
 * @param radius Specifies thickness of the sheath.
 */
Capsule::Capsule(const Point start, const Point end, float radius)
{
	radius_ = radius;
	radius_sq_ = radius*radius;
	Line line(start, end); 
	line_ = line; 
}

/**
 * @brief Used to update only the position of the capsule. The radius 
 * remains unchanged.
 * @param start Point which specifies start of the line.
 * @param end Point which specifies end of the line.
 */
void Capsule::updatePosition(const Point start, const Point end)
{
	line_=Line(start, end);
}

/**
 * @brief Detects if the specified point is within the cylinder described
 * by the capsule (i.e. does not take into account the rounded ends)
 * @param pt A Point.
 * @return True if the point is inside the cylinder.
 */
bool Capsule::PointInCylinder(const Point pt)//, Vector& return_normal, float& penetration)
{
	Vector to_pt = (Vector)pt - (Vector)line_.start;
	
	//~ std::cout << pt.x << " " << pt.y << " " << pt.z << std::endl;
	//~ std::cout << line_.start.x << " " << line_.start.y << " " << line_.start.z << std::endl;
	//~ std::cout << line_.vect.x << " " << line_.vect.y << " " << line_.vect.z << std::endl;
	//~ std::cout << to_pt.x << " " << to_pt.y << " " << to_pt.z << std::endl;
	
	float dot = to_pt.x*line_.vect.x + to_pt.y*line_.vect.y + to_pt.z*line_.vect.z;
	
	//~ std::cout << "[" << dot << "]" << std::endl;
	
    // If dot is less than zero the point is behind the pt1 cap.
	// If greater than the cylinder axis line segment length squared
	// then the point is outside the other end cap at pt2.
    if( dot < 0.0f || dot > line_.length_sq )
	{
		return false; //outside end caps
	}
	else
	{
		// Point lies within the parallel caps, so find
		// distance squared from point to line, using the fact that sin^2 + cos^2 = 1
		// the dot = cos() * |d||pd|, and cross*cross = sin^2 * |d|^2 * |pd|^2
		// Carefull: '*' means mult for scalars and dotproduct for vectors
		// In short, where dist is pt distance to cyl axis: 
		// dist = sin( pd to d ) * |pd|
		// distsq = dsq = (1 - cos^2( pd to d)) * |pd|^2
		// dsq = ( 1 - (pd * d)^2 / (|pd|^2 * |d|^2) ) * |pd|^2
		// dsq = pd * pd - dot * dot / lengthsq
		//  where lengthsq is d*d or |d|^2 that is passed into this function 

		// distance squared to the cylinder axis:	
		float dsq = (to_pt.x*to_pt.x + to_pt.y*to_pt.y + to_pt.z*to_pt.z) - dot*dot/line_.length_sq;
		//~ std::cout << "{" << dsq << "} " << radius_sq_ << std::endl;
		if( dsq > radius_sq_ )
		{
			return false; //outside edge
		}
		else
		{
			return true;		// inside cylinder (not implemented as a  capsule yet)
		}	
	}
}		

/**
 * @brief Detects if the specified point is within the capsule.
 * @param end Point which specifies end of the line.
 * @param return_normal Pointer to a Vector which returns the normal
 * from the line to the point.
 * @param closest_point Pointer to a Point which returns the closest point
 * on the centre line of the capsule.
 * @param penetration Pointer to a floating point number which returns 
 * the amount the point penetrates into the capsule as a percentage of its
 * radius.
 * @param offset I'm not sure why I put this here, but maybe I'll remember
 * at some point. If not...well it does nothing.
 * @return True if the point is within the bounds of the capsule.
 */
bool Capsule::PointInCapsule(const Point pt, Vector* return_normal, Point* closest_point, float* penetration, float offset)
{
	//get closest point on the centre line of capsule
	Vector to_pt = (Vector)pt - (Vector)line_.start;
	
	//project the point onto the centreline
	float t = to_pt.dot(line_.vect)/line_.vect.dot(line_.vect);
	
	//clamp between 0 and 1 (i.e. ensure it's within this range)
	float t_clamped = clamp(t,0,1);
	
	//find the point along the line
	Point closest_pt = line_.start + ( (Point)line_.vect * t_clamped );
	
	if (closest_point != NULL)
			*closest_point = Point(closest_pt);
	
	//thus get the shortest vector from the line to the point
	Vector shortest_vect = (Vector)closest_pt - (Vector)pt;
	
	//check if it's inside the radius
	float dot = shortest_vect.dot(shortest_vect);
	if (dot < radius_sq_)
	{
		//return the unit vector in that direction
		float magnitude = sqrt(dot);
		if (return_normal != NULL)
			*return_normal = shortest_vect/magnitude;
		if (penetration != NULL)
			*penetration = (radius_sq_-(magnitude*magnitude))/radius_sq_;
		return true;
	} else {
		return false;
	}
	
}

/**
 * @brief Creates an RViz Marker for the capsule, only shown as a cylinder
 * and not visualising the end spheres.
 * @param id ID of Rvis Marker.
 * @param base_frame A tf frame.
 * @param ns tf namespace.
 * @param colour Single colour to draw whole sheath.
 * @param alpha Floating point between 0-1 for transparency of marker.
 * @return Type visualization_msgs::Marker
 */
visualization_msgs::Marker Capsule::createRVizMarker(int id, const std::string& base_frame, const std::string& ns, char colour, float alpha)
{
	visualization_msgs::Marker marker;
	marker.header.frame_id = base_frame;
	marker.header.stamp = ros::Time();
	marker.ns = ns;
	marker.id = id;
	marker.type = visualization_msgs::Marker::CYLINDER;
	marker.action = visualization_msgs::Marker::ADD;
	
	//markers are drawn about their centre
	Point posn = line_.midpoint();
	marker.pose.position.x = posn.x;
	marker.pose.position.y = posn.y;
	marker.pose.position.z = posn.z;
	
	//default orientation is z pointing upwards
	Quaternion orient(Vector(0,0,1),Vector(posn,line_.end));
	marker.pose.orientation.x = orient.x;
	marker.pose.orientation.y = orient.y;
	marker.pose.orientation.z = orient.z;
	marker.pose.orientation.w = orient.w;

	marker.scale.x = radius_*2;
	marker.scale.y = radius_*2;
	marker.scale.z = line_.length;
	
	marker.color.a = alpha;
	marker.color.r = 0.0;
	marker.color.g = 0.0;
	marker.color.b = 0.0;
	switch(colour)
	{
		case 'r': 	marker.color.r = 1.0;
					break;
		case 'g':	marker.color.g = 1.0;
					break;
		case 'b':	marker.color.b = 1.0;
					break;
		case 'c':	marker.color.g = 1.0;
					marker.color.b = 1.0;
					break;
		case 'm':	marker.color.r = 1.0;
					marker.color.b = 1.0;
					break;
		case 'y':	marker.color.r = 1.0;
					marker.color.g = 1.0;
					break;
		case 'o':	marker.color.r = 1.0;
					marker.color.g = 0.5;
					break;
		default: 	break;
	}
	
	return marker;
}

/**
 * @brief Returns a point containing the maximum x, y and z points
 * of the whole capsule. Used, for instance, when creating an axis aligned
 * bounding box around multiple capsules (e.g. as done in CollisionSheath)
 * @param compare If True the point returned will be the maximum of the 
 * x,y,z points of the capsule and the passed point.
 * @param compare_pt A point passed for comparison if compare is true
 * @return The maximum x, y and z within one Point.
 */
Point Capsule::maxPoint(bool compare, Point compare_pt)
{
	float x = std::max(line_.start.x, line_.end.x)+radius_;
	float y = std::max(line_.start.y, line_.end.y)+radius_;
	float z = std::max(line_.start.z, line_.end.z)+radius_;
	if (compare)
	{
		x = std::max(x, compare_pt.x);
		y = std::max(y, compare_pt.y);
		z = std::max(z, compare_pt.z);
	}
	return Point(x,y,z);
}

/**
 * @brief Returns a point containing the minimum x, y and z points
 * of the whole capsule. Used, for instance, when creating an axis aligned
 * bounding box around multiple capsules (e.g. as done in CollisionSheath)
 * @param compare If True the point returned will be the minimum of the 
 * x,y,z points of the capsule and the passed point.
 * @param compare_pt A point passed for comparison if compare is true
 * @return The minimum x, y and z within one Point.
 */
Point Capsule::minPoint(bool compare, Point compare_pt)
{
	float x = std::min(line_.start.x, line_.end.x)-radius_;
	float y = std::min(line_.start.y, line_.end.y)-radius_;
	float z = std::min(line_.start.z, line_.end.z)-radius_;
	if (compare)
	{
		x = std::min(x, compare_pt.x);
		y = std::min(y, compare_pt.y);
		z = std::min(z, compare_pt.z);
	}
	return Point(x,y,z);
}

/**
 * @brief Detects an intersection between this capsule.
 * @param other_capsule The other capsule for comparison.
 * @param dist_along_this Returns the distance, from the start point, 
 * along this capsule's centreline at which the intersection occurs.
 * @param dist_along_this Returns the distance, from the start point, 
 * along the other capsule's centreline at which the intersection occurs.
 * @return True if the capsules intersect.
 */
bool Capsule::intersects(Capsule other_capsule, float* dist_along_this, float* dist_along_other, Point* point_this, Point* point_other)
{
	float dist_between_sq = line_.closestPtLineLine(other_capsule.line_, point_this, point_other, dist_along_this, dist_along_other);
	//~ float s, t;
	//~ Point c1, c2;
	//~ float dist_between_sq = line_.closestPtLineLineExact(line_.start, line_.end, other_capsule.line_.start, other_capsule.line_.end, s, t, c1, c2);
	//~ *point_this = c1;
	//~ *point_other = c2;
	//~ *dist_along_this = s;
	//~ *dist_along_other = t;
	float sum_radii = radius_+other_capsule.radius_;
	return dist_between_sq <= sum_radii*sum_radii;
}

///overloaded version of Capsule::intersects, which only confirms collision
bool Capsule::intersects(Capsule other_capsule)
{
	float dist_this, dist_other;
	Point point_this, point_other;
	return intersects(other_capsule, &dist_this, &dist_other, &point_this, &point_other);
}

Point Capsule::getStart()
{
	return line_.start;
}

Point Capsule::getEnd()
{
	return line_.end;
}

Line Capsule::getLine()
{
	return line_;
}

float Capsule::getRadius()
{
	return radius_;
}
