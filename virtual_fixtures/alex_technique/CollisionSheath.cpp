#include "alex_technique/CollisionSheath.h"

/**
 * @brief Constructor
 * @param joints A set of transform matrices, one for each joint (optional)
 * @param radius Thickness of each collision shield (optional)
 */
CollisionSheath::CollisionSheath(Pose base_pose, std::vector<TransformMatrix> joints, float radius)
{
    sheath_id_ = this; //the unique id is the pointer to this class
    assignJoints(base_pose, joints, radius);
}

/**
 * @brief Assign joints retrospectively. 
 * @param joints A set of transform matrices, one for each joint
 * @param radius Thickness of each collision shield
 */
void CollisionSheath::assignJoints(Pose base_pose, std::vector<TransformMatrix> joints, float radius)
{
    n_joints_ = (int)joints.size();
    
    if (n_joints_ <= 0)
	return;
	
    joint_angle_offsets_ = std::vector<float>(n_joints_, 0.);
    
    joint_transforms_ = joints; 
    radius_ = radius;
    
    jacobian_.setNjoints(n_joints_);
    
    TransformMatrix running_transform = TransformMatrix();
    
    //shift to base position
    base_pose_transform_ = TransformMatrix(base_pose.posn, base_pose.rot_x, base_pose.rot_y, base_pose.rot_z);
    running_transform = running_transform*base_pose_transform_;
    
    for (int i = 0; i < n_joints_; i++)
    {
	//~ running_transforms_.push_back(running_transform);
	joint_posns_.push_back(running_transform.getPosn());

	//~ jacobian_.updateJoint(i, joint_posns_[i], running_transform.getZaxis());
	
	running_transform = running_transform*joint_transforms_[i];
	
	jacobian_.updateJoint(i, joint_posns_[i], running_transform.getZaxis());
	
	//debug
	running_transforms_.push_back(running_transform);
	
	//make capsules between joints
	if (i>0)
	{
	    capsules_.push_back(Capsule(joint_posns_[i-1],joint_posns_[i], radius_));
	    
	    //set a loose bounding box around the whole robot for rough checking
	    if (i == 1)
	    {
		max_point_ = capsules_[0].maxPoint(false);
		min_point_ = capsules_[0].minPoint(false);
	    } else {
		max_point_ = capsules_[i-1].maxPoint(true, max_point_);
		min_point_ = capsules_[i-1].minPoint(true, min_point_);
	    }
	}
    } 
    n_capsules_ = (int)capsules_.size();
}

void CollisionSheath::setOffsets(std::vector<float> offsets)
{
    if ((int)offsets.size() != n_joints_)
    {
	std::cerr << "Warning. Wrong number of Joint Angle Offsets (Needed " << n_joints_ << " and given " << offsets.size() << "). Exiting" << std::endl;
	return;
    }
    
    joint_angle_offsets_ = offsets;
}
/**
 * @brief For revolute joints (currently only supports revolute).
 * The angles are updated. 
 * @param angles An array of floating point angles, must contain the 
 * same number as there are joints.
 */
void CollisionSheath::updateJointAngles(std::vector<float> angles)
{    
    if ((int)angles.size() != n_joints_)
    {
	std::cerr << "Warning. Wrong number of Joints (Needed " << n_joints_ << " and given " << angles.size() << "). Exiting" << std::endl;
	return;
    }
    
    TransformMatrix running_transform = TransformMatrix();
    running_transform = running_transform*base_pose_transform_;
    for (int i = 0; i < n_joints_; i++)
    {
	//~ running_transforms_.push_back(running_transform);
	joint_posns_[i] = running_transform.getPosn();

	//~ jacobian_.updateJoint(i, joint_posns_[i], running_transform.getZaxis());

	joint_transforms_[i].setTheta(angles[i]+joint_angle_offsets_[i]);
	running_transform = running_transform*joint_transforms_[i];
	
	jacobian_.updateJoint(i, joint_posns_[i], running_transform.getZaxis());
	
	//debug
	running_transforms_.push_back(running_transform);
    
	//~ std::ostringstream out;
	//~ for (int j = 0; j < (int)capsules_.size(); j++)
	//~ {
	    //~ Point start = joint_posns_[j-1];//capsules_[i].getStart();
	    //~ Point end = joint_posns_[j];//capsules_[i].getEnd();
		//~ out << "[" << start.x << "," << start.y;
		//~ out << "," << start.z << "]";
		//~ out << "(" << end.x << "," << end.y;
		//~ out << "," << end.z << ")";
		//~ out << "\n";
	//~ }
	//~ out << "\n";
	//~ std::cout << out.str();
	
	//make capsules between joints
	if (i>0)
	{
	    capsules_[i-1].updatePosition(joint_posns_[i-1],joint_posns_[i]);
	
	    //set a loose bounding box around the whole robot for rough checking
	    if (i == 1)
	    {
		max_point_ = capsules_[0].maxPoint();
		min_point_ = capsules_[0].minPoint();
	    } else {
		max_point_ = capsules_[i-1].maxPoint(true, max_point_);
		min_point_ = capsules_[i-1].minPoint(true, min_point_);
	    }
	}
    } 
}

/**
 * @brief Returns the position of the specified joint.
 * @param joint_num The index of the joint. Must be valid.
 * @return A point specifying centroid of the joint.
 */
Point CollisionSheath::getJointPosition(int joint_num)
{
    if (joint_num >= n_joints_ || joint_num < 0)
    {
	std::cerr << "Warning. Invalid Joint Number. Exiting" << std::endl;
	return Point();
    }
    
    return joint_posns_[joint_num];
}

/**
 * @brief Detects if there is a collision between 2 collision sheaths.
 * This function only has to be run once for each pair of collision sheaths.
 * It doesn't matter which order.
 * @param other_sheath The other collision sheath.
 * @param link_this A pointer to an int which returns which link, in this
 * collision sheath, has collided. Returns -1 if there was no collision. (optional)
 * @param link_other A pointer to an int which returns which link, on the
 * other collision sheath, has collided. Returns -1 if there was no collision. (optional)
 * @param dist_along_this A pointer to a float which returns how far along
 * this link the collision occurred. Returns -1 if there was no collision. (optional)
 * @param dist_along_other A pointer to a float which returns how far along
 * the other link the collision occurred. Returns -1 if there was no collision. (optional)
 * @return True if there was a collision. False if not. 
 */
bool CollisionSheath::detectCollision(CollisionSheath& other_sheath, int* link_this, int* link_other, float* dist_along_this, float* dist_along_other, Point* point_along_this, Point* point_along_other)
{
    //load up the return values with -1
    if (link_this)
	*link_this = -1;
    if (link_other)
	*link_other = -1;
    if (dist_along_this)
	*dist_along_this = -1;
    if (dist_along_other)
	*dist_along_other = -1;
	
    //run a quick collision test with the axis aligned boxes (AABBs)
    
    //return if AABBs don't overlap (from Ericson 2005, p.79)
    if (max_point_.x < other_sheath.min_point_.x || min_point_.x > other_sheath.max_point_.x) return false;
    if (max_point_.y < other_sheath.min_point_.y || min_point_.y > other_sheath.max_point_.y) return false;
    if (max_point_.z < other_sheath.min_point_.z || min_point_.z > other_sheath.max_point_.z) return false;
    
    //If we're here, AABBs intersect
    
    //now cycle through the capsules (from last, tip, to first, base)
    //(the reason is that all previous joints are involved in a collision
    // at the tip but none of the following joints)
    float dist_this, dist_other; //temporary floats to pass to interects function
    Point point_this, point_other;
    for (int i = n_capsules_-1; i >= 0; i--)
    {
	for (int j = other_sheath.n_capsules_-1; j >= 0; j--)
	{
	    //~ std::cout << "[" << i << "]{" << j << "}" << std::endl;
	    if (capsules_[i].intersects(other_sheath.capsules_[j], &dist_this, &dist_other, &point_this, &point_other))
	    {
		//to return which links are colliding
		if (link_this)
		    *link_this = i;
		if (link_other)
		    *link_other = j;
		
		//to return how far along links they collided
		if (dist_along_this)
		    *dist_along_this = dist_this;
		if (dist_along_other)
		    *dist_along_other = dist_other;
		if (point_along_this)
		    *point_along_this = point_this;
		if (point_along_other)
		    *point_along_other = point_other;
		
		Line pt2pt(point_this, point_other);
		//~ std::cout << "vlength: " << pt2pt.length;
		float penetration = ((radius_+other_sheath.radius_) - pt2pt.length)/pt2pt.length;
		//~ std::cout << " penetration: " << penetration;
		Vector this_dir  = -pt2pt.vect*penetration;
		Vector other_dir = pt2pt.vect*penetration;
		//~ std::cout << " newlength: " << this_dir.length() << std::endl;
		
		//update the CollisionData structs in both Collision Sheaths
		addCollision(true, CollisionData(true, i, dist_this, this_dir, other_sheath.sheath_id_));
		other_sheath.addCollision(true, CollisionData(true, j, dist_other, other_dir, sheath_id_));
		
		//Add for visualisation of force.
		addForceArrow(point_this, this_dir);
		other_sheath.addForceArrow(point_other, other_dir);

		return true; //there has been a collision
	    }
	}
    }
    //no collision was found between capsules but add it anyway
    addCollision(false, CollisionData(false, 0, 0, Vector(), other_sheath.sheath_id_));
    other_sheath.addCollision(false, CollisionData(false, 0, 0, Vector(), sheath_id_));
    return false;
}

/**
 * @brief Adds a CollisionData object to the vector of collisions_ between
 * known CollisionSheath instances. If a collision occurs between an unknown
 * CollisionSheath instance, this is added to collisions_. If it is known, 
 * then the existing data is updated. Used in calculateTorques.
 * @param collision True if a collision occurred between the 2 sheaths. 
 * False if no collision - must be updated to overwrite any previous collisions.
 * @param new_collision A CollisionData with information on the link, distance 
 * along that link and CollisionSheath unique identifier.
 */
void CollisionSheath::addCollision(bool collision, CollisionData new_collision)
{
    for (size_t i = 0; i < collisions_.size(); i++)
    {
	//update any existing collision with the same sheath
	if (new_collision.sheath_id == collisions_[i].sheath_id)
	{
	    collisions_[i] = new_collision;
	    return;
	}
    }
    //if we're here this is a previously unknown sheath
    collisions_.push_back(new_collision);    
}

/**
 * @brief Creates an array of Markers which can be visualised in Rviz.
 * @param id ID of Rvis Marker.
 * @param base_frame A tf frame.
 * @param ns tf namespace.
 * @param colour Single colour to draw whole sheath.
 * @param alpha Floating point between 0-1 for transparency of marker.
 * @param show_bounding_box. If true, an axis aligned bounding box of 
 * 1/3 transparency will be shown around the sheath.
 * @return Type visualization_msgs::MarkerArray
 */
visualization_msgs::MarkerArray CollisionSheath::createRVizMarkers(int id, const std::string& base_frame, const std::string& ns, char colour, float alpha, bool show_bounding_box, bool show_force_arrows)
{
    visualization_msgs::MarkerArray markers;

    for (int i = 0; i < n_capsules_; i++)
    {
	markers.markers.push_back(capsules_[i].createRVizMarker(id*10+i, base_frame, ns, colour, alpha));
    }
    if (show_bounding_box)
    {
	markers.markers.push_back(createBoundingMarker(id*10+n_capsules_, base_frame, ns, colour, alpha/3));
    }
    
    if (show_force_arrows)
    {
	std::vector<visualization_msgs::Marker> forcearrows = getForceArrows(id*10+n_capsules_+1, base_frame, ns, true);
	for (size_t i = 0; i < forcearrows.size(); i++)
	    markers.markers.push_back(forcearrows[i]);
    }
    
    return markers;
}

void CollisionSheath::addForceArrow(Point start, Vector direction)
{
    visualization_msgs::Marker marker;
    
    marker.type = visualization_msgs::Marker::ARROW;
    marker.action = visualization_msgs::Marker::ADD;
    
    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;
    marker.color.a = 1.0;
    
    marker.scale.x = 0.01;
    marker.scale.y = 0.04;
    
    geometry_msgs::Point pt;
    
    pt.x = start.x;
    pt.y = start.y;
    pt.z = start.z;
    marker.points.push_back(pt);
    
    Point end = start + (Point)direction*direction.length()*100;
    pt.x = end.x;
    pt.y = end.y;
    pt.z = end.z;
    marker.points.push_back(pt);
    
    force_arrows_.push_back(marker);
}

std::vector<visualization_msgs::Marker> CollisionSheath::getForceArrows(int id, const std::string& base_frame, const std::string& ns, bool reset)
{
    std::vector<visualization_msgs::Marker> returnmarkers = force_arrows_;
    
    for (size_t i = 0; i < returnmarkers.size(); i++)
    {
	returnmarkers[i].header.frame_id = base_frame.c_str();
	returnmarkers[i].header.stamp = ros::Time();
    
	returnmarkers[i].id = id+(int)i;
	returnmarkers[i].ns = ns;
    }
    
    if (reset)
	force_arrows_ = std::vector<visualization_msgs::Marker>();
	
    return returnmarkers;
}

/**
 * @brief Overloaded version of createRVizMarkers to make creation of
 * many robots easier. Selects the colour automatically based on 
 * robot_number and sets it in the reference frame "world"
 * @param robot_number The index of the robot
 * @param base_frame A tf frame.
 * @param alpha Floating point between 0-1 for transparency of marker.
 * @param show_bounding_box. If true, an axis aligned bounding box of 
 * 1/3 transparency will be shown around the sheath.
 * @return Type visualization_msgs::MarkerArray
 */
visualization_msgs::MarkerArray CollisionSheath::createRVizMarkers(int robot_number, float alpha, bool show_bounding_box, bool show_force_arrows)
{
    std::ostringstream robotname;
    robotname << "robot_" << robot_number;
    
    //automatically set the colour (7 possible) of the robot based on its number
    char colour;
    switch(robot_number%7)
    {
	case 0: colour = 'r';
		 break;
	case 1: colour = 'g';
		 break;
	case 2: colour = 'b';
		 break;
	case 3: colour = 'c';
		break;
	case 4: colour = 'm';
		 break;
	case 5: colour = 'y';
		 break;
	case 6: colour = 'o';
		break;
	default: colour = 'k';
		  break;
    }
    return createRVizMarkers(robot_number,"world",robotname.str(),colour, alpha, show_bounding_box, show_force_arrows);
}

/**
 * @brief Creates a single axis aligned bounding box around the sheath 
 * which can be visualised in Rviz.
 * @param id ID of Rvis Marker.
 * @param base_frame A tf frame.
 * @param ns tf namespace.
 * @param colour Colour to draw the box
 * @param alpha Floating point between 0-1 for transparency of marker.
 * @return Type visualization_msgs::Marker
 */
visualization_msgs::Marker CollisionSheath::createBoundingMarker(int id, const std::string& base_frame, const std::string& ns, char colour, float alpha)
{
	visualization_msgs::Marker marker;
	marker.header.frame_id = base_frame.c_str();
	marker.header.stamp = ros::Time();
	marker.ns = ns.c_str();
	marker.id = id;
	marker.type = visualization_msgs::Marker::CUBE;
	marker.action = visualization_msgs::Marker::ADD;
	
	//markers are drawn about their centre
	Point posn = (max_point_+min_point_)/2;
	marker.pose.position.x = posn.x;
	marker.pose.position.y = posn.y;
	marker.pose.position.z = posn.z;
	
	//default orientation is z pointing upwards
	marker.pose.orientation.x = 0;
	marker.pose.orientation.y = 0;
	marker.pose.orientation.z = 0;
	marker.pose.orientation.w = 1;

	marker.scale.x = max_point_.x - min_point_.x;
	marker.scale.y = max_point_.y - min_point_.y;
	marker.scale.z = max_point_.z - min_point_.z;
	
	marker.color.a = alpha;
	marker.color.r = 0.0;
	marker.color.g = 0.0;
	marker.color.b = 0.0;
	switch(colour)
	{
		case 'r': 	marker.color.r = 1.0;
					break;
		case 'g':	marker.color.g = 1.0;
					break;
		case 'b':	marker.color.b = 1.0;
					break;
		case 'c':	marker.color.g = 1.0;
				marker.color.b = 1.0;
					break;
		case 'm':	marker.color.r = 1.0;
				marker.color.b = 1.0;
					break;
		case 'y':	marker.color.r = 1.0;
				marker.color.g = 1.0;
					break;
		case 'o':	marker.color.r = 1.0;
				marker.color.g = 0.5;
					break;
		default: 	break;
	}
	
	return marker;
}

std::vector<float> CollisionSheath::getTorques(float stiffness, float min_torque, float max_torque)
{
    std::vector<float> combined_torques(n_joints_, 0.);
    for (size_t i = 0; i < collisions_.size(); i++)
    {
	//if there actually was a collision
	if (collisions_[i].collision)
	{
	    //grab out the jacobian up to the link of the collision
	    JacobianMatrix jac = jacobian_.getSubMatrix(0, collisions_[i].link);

	    //add a new link which ends at the point of collision and calculate
	    //the jacobian of the resulting "virtual robot"
	    TransformMatrix virtual_link(collisions_[i].dist, 0., 0., 0.);
	    TransformMatrix virtual_robot = running_transforms_[collisions_[i].link]*virtual_link;
	    jac.appendJoint(virtual_robot.getPosn(), virtual_robot.getZaxis());
	    jac.populate(); //calculate jacobian

	    //multiply force vector by the stiffness
	    Vector trans_force = collisions_[i].vect*stiffness;
	    //~ Vector trans_force = collisions_[i].vect;

	    std::vector<float> torques = jac.applyWrench(trans_force, Vector()); //NOTE: rotation has no value yet
	    
	    //~ for (size_t j = 0; j < torques.size(); j++)
		//~ torques[j] *= stiffness;
		
	    //~ std::cout << "\t\t\t\t\t\t\t\t\t\t";
	    //~ std::cout << "Link = " << collisions_[i].link << " Dist = " << collisions_[i].dist << std::endl;
	    //~ std::cout << "\t\t\t\t\t\t\t\t\t\t";
	    //~ std::cout << "Force (x,y,z) = (" << trans_force.x << "," << trans_force.y << "," << trans_force.z << ")\n";
	    if (jac.n_joints > n_joints_)
	    {
		std::cerr << "Error! Jacobian computation has too many joints" << std::endl;
		return std::vector<float>();
	    }
	    
	    //add the torques applied by this collision to the robot joints
	    //NOTE. This will have an additive effect (i.e. if two other arms
	    //collide with this one simultaneously the force applied to this
	    //one will be the sum of the forces applied by the other arms.
	    //perhaps this will be wrong. testing may determine if so.
	    for (int j = 0; j < jac.n_joints; j++)
		combined_torques[j] += torques[j];
	}
    }
    
    
    //~ std::cout << "preclamp  (";
    //~ for (size_t j = 0; j < combined_torques.size(); j++)
	//~ std::cout << combined_torques[j] << ",";
    //~ std::cout << ")" << std::endl;
    
    //clamp the torques to legal values
    for (int i = 0; i < n_joints_; i++)
	combined_torques[i] = combined_torques[i] < min_torque ? min_torque : ( combined_torques[i] > max_torque ? max_torque : combined_torques[i] );

    return combined_torques;
}

//////////////////////////////Kraft Specific///////////////////////////////////////
/**
 * @brief Creates a CollisionSheath with the properties of one Kraft arm.
 * @param base_posn A Point for the centre of the base.
 * @param rot_x Fixed rotation of the base frame. Floating point.
 * @param rot_y Fixed rotation of the base frame. Floating point.
 * @param rot_z Fixed rotation of the base frame. Floating point.
 */
KraftCollisionSheath::KraftCollisionSheath(Point base_posn, float rot_x, float rot_y, float rot_z)
{
    KraftCollisionSheath(Pose(base_posn, rot_x, rot_y, rot_z));
}

KraftCollisionSheath::KraftCollisionSheath(Pose base_pose)
{    
    std::vector<TransformMatrix> transforms;
    
    const float radius = 0.1;

    //SA
    transforms.push_back(TransformMatrix(0.353,	0,	0, 	M_PI_2));
    //SE
    transforms.push_back(TransformMatrix(0,	0,	0.522,	0));
    //EL
    transforms.push_back(TransformMatrix(0,	0,	0.261,	0));
    //~ transforms.push_back(TransformMatrix(0,	0,	0.5,	0));
    //WP
    transforms.push_back(TransformMatrix(0,	0,	0.133, 	-M_PI_2));
    //~ transforms.push_back(TransformMatrix(0,	0,	1, 	-M_PI_2));
    //WY
    transforms.push_back(TransformMatrix(0.035,	0,	0, 	M_PI_2));
    //WR
    transforms.push_back(TransformMatrix(0.389,	0,	0,	0));
    //~ //SA
    //~ transforms.push_back(TransformMatrix(1.0,	0,	0, 	M_PI_2));
    //~ //SE
    //~ transforms.push_back(TransformMatrix(0,	0,	1.0,	0));
    //~ //EL
    //~ transforms.push_back(TransformMatrix(0,	0,	1.0,	0));
    //~ //WP
    //~ transforms.push_back(TransformMatrix(0,	0,	1.0, 	-M_PI_2));
    //~ //WY
    //~ transforms.push_back(TransformMatrix(1.0,	0,	0, 	M_PI_2));
    //~ //WR
    //~ transforms.push_back(TransformMatrix(1.0,	0,	0,	0));

    assignJoints(base_pose, transforms, radius);
    
    float offsets[] = {0, M_PI_2, -M_PI_2, 0, 0, 0};
    setOffsets(std::vector<float>(offsets, offsets+sizeof(offsets)/sizeof(float)));
}
