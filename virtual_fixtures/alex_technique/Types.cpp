#include "alex_technique/Types.h"

// ----------------------________________------------------------------
// ----------------------     Point      ------------------------------
// ----------------------________________------------------------------

Point::Point(float x_, float y_, float z_)
{
    x = x_; 
    y = y_; 
    z = z_;
}

Point Point::operator+(const Point pt) const
{
    return Point(x+pt.x, y+pt.y, z+pt.z);
}

Point Point::operator-(const Point pt) const  
{
    return Point(x-pt.x, y-pt.y, z-pt.z);
}

Point Point::operator*(const Point pt) const  
{
    return Point(x*pt.x, y*pt.y, z*pt.z);
}

Point Point::operator*(float scalar) const  
{
    return Point(x*scalar, y*scalar, z*scalar);

} 

Point Point::operator/(float scalar) const  
{
    return Point(x/scalar, y/scalar, z/scalar);
}
		
float Point::distanceSquared(const Point pt) const 
{
	Point a(x-pt.x, y-pt.y, z-pt.z);
	return a.x*a.x + a.y*a.y + a.z*a.z;
}
		
float Point::distanceTo(const Point pt) const  
{ 
    return sqrt(distanceSquared(pt)); 
}

// ----------------------________________------------------------------
// ----------------------     Vector     ------------------------------
// ----------------------________________------------------------------
Vector::Vector(float x_, float y_, float z_)
{
    x = x_; 
    y = y_; 
    z = z_;
}

Vector::Vector(const Point A, const Point B)
{
    x = B.x-A.x; y = B.y-A.y; z = B.z-A.z;
}	

Vector::Vector(const Point pt)
{
    x = pt.x; y = pt.y; z = pt.z;
}

Vector Vector::operator*(const  Vector vec) const  
{
    return Vector(x*vec.x, y*vec.y, z*vec.z);
}

Vector Vector::operator-(const  Vector vec) const  
{
    return Vector(x-vec.x, y-vec.y, z-vec.z);
}

Vector Vector::operator*(float scalar) const  
{
    return Vector(x*scalar, y*scalar, z*scalar);

} 

Vector Vector::operator-() const
{
	return Vector(-x, -y, -z);
}

float Vector::length_sq() const 
{
    return x*x+y*y+z*z;
}

float Vector::length() const 
{
    return sqrt(length_sq());
}

float Vector::distanceTo(const Point pt) const  
{
	Point a(x-pt.x, y-pt.y, z-pt.z);
	return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}

float Vector::dot(const Vector vect) const  
{ 
    return x*vect.x + y*vect.y + z*vect.z; 
}

Vector Vector::cross(const Vector vect) const  
{ 
    //~ return Vector(y*vect.z-z*vect.y,-(x*vect.z-z*vect.x),x*vect.y-y*vect.x); 
    return Vector(y*vect.z-z*vect.y,z*vect.x-x*vect.z,x*vect.y-y*vect.x); 
}

// ----------------------________________------------------------------
// ----------------------  Quaternion    ------------------------------
// ----------------------________________------------------------------
Quaternion::Quaternion(Vector vect1, Vector vect2)
{
	//quaternion to move from vect1 to vect2
	Vector a = vect1.cross(vect2);
	x = a.x; y = a.y; z = a.z;
	w = sqrt(vect1.length_sq()*vect2.length_sq()) + vect1.dot(vect2);
}

Quaternion Quaternion::operator*(const Quaternion total) const 
{
	Quaternion new_total;
	new_total.x = w*total.x + x*total.w + y*total.z - z*total.y;
	new_total.y = w*total.y - x*total.z + y*total.w - z*total.x;
	new_total.z = w*total.z + x*total.y - y*total.x + z*total.w;
	new_total.w = w*total.w - x*total.x - y*total.y - z*total.z;
	return new_total;
}			

void Quaternion::normalise()
{
	//normalise if different enough from a unit vector
	float mag2 = w*w +x*x + y*y + z*z;
	if (!isZero(mag2) && !isZero(mag2 - 1.0f))
	{
		float mag = sqrt(mag2);
		w /= mag;
		x /= mag;
		y /= mag;
		z /= mag;
	}
}

// ----------------------________________------------------------------
// ----------------------     Pose       ------------------------------
// ----------------------________________------------------------------

Pose::Pose(Point posn_, float rot_x_, float rot_y_, float rot_z_)
{
	posn = posn_;
	rot_x = rot_x_;
	rot_y = rot_y_;
	rot_z = rot_z_;
}

// ----------------------________________------------------------------
// ----------------------     Line       ------------------------------
// ----------------------________________------------------------------

Line::Line(const Point start_, const Point end_)
{
	start = start_; end = end_;
	length_sq = start.distanceSquared(end);
	length = std::sqrt(length_sq); 
	vect = (Vector)end-start;
} 

///@return The Point lying at the midpoint of this line.
Point Line::midpoint() const 
{
    return (end+start)/2;
}

//~ ///@brief Calculates the length by taking the sqrt of length_sq
//~ ///@return The length of this line
//~ float Line::length() const   
//~ { 
    //~ return std::sqrt(length_sq); 
//~ }

///Calculates the squared distance from a point to this line
float Line::sqDistanceTo(const Point pt) const 
{
	Vector start2pt = (Vector)pt - (Vector)start;
	float dot = vect.dot(start2pt);
	//if point outside line segment
	if (dot <= 0.0f) return start2pt.dot(start2pt);
	float ddot = vect.dot(vect);
	if (dot >= ddot)
	{ 
		Vector end2pt = (Vector)pt - (Vector)end;
		return end2pt.dot(end2pt);
	}
	//when pt projects on the line
	return start2pt.dot(start2pt) - dot*dot/ddot;
}

/**
 * @brief Returns the closest points on two lines to each other. 
 * Algorithm from Ericson, C., 2005. Real-Time Collision Detection, San Francisco, CA, USA:
 *  Morgan Kaufmann Pubishers, Elsevier. pp. 150
 * @param other_line Another line instance.
 * @param pt_this A pointer to a Point which returns the closest point on this line.
 * @param pt_other A pointer to a Point which returns the closest point on the other line.
 * @param dist_along_this A pointer to a float which returns the distance which pt_this lies along
 * this line (from start to end).
 * @param dist_along_other A pointer to a float which returns the distance which pt_other lies along
 * the other line (from start to end).
 * @return The square of the smallest distance between the two lines.
*/
float Line::closestPtLineLine(Line other_line, Point* pt_this, Point* pt_other, float* dist_along_this, float* dist_along_other) const 
{
	//return values
	float distance_sq;
	float s = 0.0; //distance along this line
	float t = 0.0; //distance along the other line
	
	//calculate vector betwen line start points
	Vector r = (Vector)start - (Vector)other_line.start;
	float f = other_line.vect.dot(r);
	
	//~ std::cout << "len_sq: " << length_sq << " [vectdot]: " << vect.dot(vect) << std::endl; 
	//~ std::cout << "other_len_sq: " << other_line.length_sq << " [othervectdot]: " << other_line.vect.dot(other_line.vect) << std::endl; 
	
	//check if either or both segments degenerate into points 
	//(i.e. their length is really small)
	if (isZero(length_sq) && isZero(other_line.length_sq))
	{	
		//both segments degenerate to a point 
		s = t = 0.0;
		*pt_this = start; 
	    *pt_other = other_line.start;
	}
	else
	{
		if (isZero(length_sq))
		{
			//first segment degenerates to a point
			s = 0.0;
			t = f / other_line.length_sq;
			t = clamp(t, 0.0, 1.0);
		} 
		else 
		{
			float c = vect.dot(r);
			if (isZero(other_line.length_sq))
			{
				//second segment degenerates to a point
				t = 0.0;
				s = clamp(-c/length_sq, 0.0, 1.0);
			}
			else
			{
				//general case (neither segment degenerates)
				float b = vect.dot(other_line.vect);
				float denom = length_sq*other_line.length_sq - b*b;
				//~ std::cout << "B: " << b << " Denom: " << denom << std::endl; 
				//~ std::cout << "F: " << f << " C: " << c << std::endl; 
				//~ std::cout << "len: " << length_sq << " olen: " << other_line.length_sq << std::endl; 
				//~ std::cout << "(s1: " << s << ")";
				//~ std::cout << "[t1: " << t << "]";
				
				//if segments not parallel, compute closest point
				// and clamp to this line. else pick abitrary s (0)
				if (denom != 0.0)
				{
					s = clamp((b*f - c*other_line.length_sq)/denom, 0.0,1.0);
					//~ std::cout << "CALC";
					//~ std::cout << "(s2: " << s << ")";
					//~ std::cout << "[t2: " << t << "]";
				} 
				else 
				{
					s = 0.0;
				}
				//~ std::cout << "(s3: " << s << ")";
				//~ std::cout << "[t3: " << t << "]";
				//compute closest point on other line
				t = (b*s + f) / other_line.length_sq;
				//~ std::cout << "(s4: " << s << ")";
				//~ std::cout << "[t4: " << t << "]";
				//ensure t is within the other line (between 0 & 1)
				if (t < 0.0)
				{
					t = 0.0;
					s = clamp(-c/length_sq, 0.0, 1.0);
					//~ std::cout << "{{t<0.0}}";
				}
				else if (t > 1.0)
				{
					t = 1.0;
					s = clamp((b-c)/length_sq, 0.0, 1.0);
					//~ std::cout << "{{t>1.0}}";
				}
				//~ std::cout << "(s5: " << s << ")"; 
				//~ std::cout << "[t5: " << t << "]" << std::endl;
			}
		}
		*pt_this = start + (Point)vect * s;
		*pt_other = other_line.start + (Point) other_line.vect * t;
		//~ std::cout << "THIS = " << s << " OTHER = " << t << std::endl;
	}
	Vector between_pts = (Vector)*pt_this - (Vector)*pt_other;
	distance_sq = between_pts.dot(between_pts);
	
	*dist_along_this = s*length;
	*dist_along_other = t*other_line.length;			
	
	return distance_sq;
}

float Line::closestPtLineLineExact(Point p1, Point q1, Point p2, Point q2, float &s, float &t, Point &c1, Point &c2)
{
	Vector d1 = (Vector)q1 - (Vector)p1; // Direction vector of segment S1 
	Vector d2 = (Vector)q2 - (Vector)p2; // Direction vector of segment S2 
	Vector r = (Vector)p1 - (Vector)p2; 
	float a = d1.dot(d1); // Squared length of segment S1, always nonnegative 
	float e = d2.dot(d2); // Squared length of segment S2, always nonnegative 
	float f = d2.dot(r);
	// Check if either or both segments degenerate into points 
	if (a <= 0.0001 && e <= 0.0001) { 
		// Both segments degenerate into points 
		s = t = 0.0f; 
		c1 = p1; 
		c2 = p2;
		Vector c1c2 = (Vector)c1-(Vector)c2;
		return c1c2.dot(c1c2); 
		}
	if (a <= EPSILON) { 
			// First segment degenerates into a point 
			s = 0.0f; 
			t = f/e; //s=0=>t= (b*s+f)/e=f/e 
			t = clamp(t, 0.0f, 1.0f);
	} else { 
			float c = d1.dot(r); 
			if (e <= 0.0001) { 
				// Second segment degenerates into a point 
				t = 0.0f; 
				s = clamp(-c / a, 0.0f, 1.0f); //t=0=>s= (b*t-c)/a=-c/a
			} else { 
				// The general nondegenerate case starts here 
				float b = d1.dot(d2); 
				float denom = a*e-b*b; // Always nonnegative

				// If segments not parallel, compute closest point on L1 to L2 and 
				// clamp to segment S1. Else pick arbitrary s (here 0) 
				if (denom != 0.0f) { 
					s = clamp((b*f - c*e) / denom, 0.0f, 1.0f);
				} else s = 0.0f; 
				// Compute point on L2 closest to S1(s) using 
				// t = Dot((P1 + D1*s) - P2,D2) / Dot(D2,D2) = (b*s + f) / e 
				t=(b*s+f)/e;
				
				// If t in [0,1] done. Else clamp t, recompute s for the new value 
				// of t using s = Dot((P2 + D2*t) - P1,D1) / Dot(D1,D1)= (t*b - c) / a 
				// and clamp s to [0, 1] i
				if (t < 0.0f) { 
					t = 0.0f; 
					s = clamp(-c / a, 0.0f, 1.0f);
				} else if (t > 1.0f) { 
					t = 1.0f; 
					s = clamp((b - c) / a, 0.0f, 1.0f);
				}
			}
		}
		
		c1=p1+(Point)d1*s; 
		c2=p2+(Point)d2*t; 
		Vector c1c2 = (Vector)c1-(Vector)c2;
		return c1c2.dot(c1c2); 
}

///overloaded version of closestPtLineLine, which only returns distance squared
float Line::closestPtLineLine(Line other_line) const 
{
	Point thispt, otherpt;
	float dist_this, dist_other;
	return closestPtLineLine(other_line, &thispt, &otherpt, &dist_this, &dist_other );
}
		
		
// ----------------------________________------------------------------
// ----------------------TransformMatrix ------------------------------
// ----------------------________________------------------------------
TransformMatrix::TransformMatrix(bool identity)
{
	if (identity)
		setIdentity();	
	else
		data_.assign(16, 0); //set to zeros
} 

TransformMatrix::TransformMatrix(Point position, float rot_x, float rot_y, float rot_z) //translation only
{
	setIdentity();	
	//translation
	data_[3] = position.x; 
	data_[7] = position.y;
	data_[11] = position.z;	
	
	if (rot_x != 0 || rot_y != 0 || rot_z != 0)
	{
		float calpha = cos(rot_x);
		float salpha = sin(rot_x);
		float cbeta = cos(rot_y);
		float sbeta = sin(rot_y);
		float cgamma = cos(rot_z);
		float sgamma = sin(rot_z);
		
		//rotation
		data_[0] = cbeta*cgamma;
		data_[1] = -cbeta*sgamma;
		data_[2] = sbeta;
		data_[4] = calpha*sgamma+salpha*sbeta*cgamma;
		data_[5] = calpha*cgamma-salpha*sbeta*sgamma;
		data_[6] = -salpha*cbeta;
		data_[8] = salpha*sgamma-calpha*sbeta*cgamma;
		data_[9] = salpha*cgamma+calpha*sbeta*sgamma;
		data_[10] = calpha*cbeta;
				
	}
}

/**
 * @brief Constructor which creates a Transform Matrix from the Denavit-Hartenberg
 * Parameters. 
 * @param d Joint Offset
 * @param theta Joint Angle
 * @param a Link Length
 * @param alpha Twist Angle
 */ 
TransformMatrix::TransformMatrix(float d, float theta, float a, float alpha)
{
	setIdentity();	
	
	stheta_ = sin(theta);
	ctheta_ = cos(theta);
	salpha_ = sin(alpha);
	calpha_ = cos(alpha);
	a_ = a;
	
	//RPR
	//rotation
	data_[0] = ctheta_;
	data_[1] = -stheta_*calpha_;
	data_[2] = stheta_*salpha_;
	data_[4] = stheta_;
	data_[5] = ctheta_*calpha_;
	data_[6] = -ctheta_*salpha_;
	data_[8] = 0;
	data_[9] = salpha_;
	data_[10] = calpha_;
	
	//translation
	data_[3] = a_*ctheta_;
	data_[7] = a_*stheta_;
	data_[11] = d;	
}

TransformMatrix TransformMatrix::operator*(const TransformMatrix mat) const 
{
	TransformMatrix new_mat(false);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			for (int k = 0; k < 4; k++)
			{
				new_mat.data_[4*i+j] += data_[4*i+k]*mat.data_[4*k+j];
			}
		}			
	}
	return new_mat;
}

void TransformMatrix::setIdentity()
{
	data_.assign(16, 0);
	data_[0] = data_[5] = data_[10] = data_[15] = 1;		
}

void TransformMatrix::setTheta(float theta) //rotational actuator
{
	stheta_ = sin(theta);
	ctheta_ = cos(theta);
	
	//RPR
	//rotation
	data_[0] = ctheta_;
	data_[1] = -stheta_*calpha_;
	data_[2] = stheta_*salpha_;
	data_[4] = stheta_;
	data_[5] = ctheta_*calpha_;
	data_[6] = -ctheta_*salpha_;
	
	//translation
	data_[3] = a_*ctheta_;
	data_[7] = a_*stheta_;	
}

void TransformMatrix::setD(float d) // linear actuator
{
	data_[11] = d;
}

Point TransformMatrix::getPosn() const 
{
	return Point(data_[3],data_[7],data_[11]);
}

Vector TransformMatrix::getZaxis() const
{
	return Vector(data_[2], data_[6], data_[10]);
}

// ----------------------________________------------------------------
// ----------------------Jacobian Matrix ------------------------------
// ----------------------________________------------------------------

JacobianMatrix::JacobianMatrix(int num_joints)
{
	setNjoints(num_joints);
}

JacobianMatrix::JacobianMatrix(std::vector<Point> positions, std::vector<Vector> zaxes)
{
	if (zaxes.size() != positions.size())
	{
		std::cerr << "Error! zaxes and positions are not the same size." << std::endl;
		return;
	}
	n_joints = (int)zaxes.size();
	zaxes_ = zaxes;
	positions_ = positions;
}

void JacobianMatrix::setNjoints(int num_joints)
{
	n_joints = num_joints;
	
	if (n_joints > 0)
	{
		data_.assign(6*n_joints, 0.);
		positions_.assign(n_joints, 0.);
		zaxes_.assign(n_joints, 0.);
	}
}

void JacobianMatrix::updateJoint(int joint_num, Point position, Vector zaxis)
{
	if (joint_num < n_joints)
	{
		positions_[joint_num] = position;
		zaxes_[joint_num] = zaxis;
	}
	else
	{
		std::cerr << "WARNING (JacobianMatrix::updateJoint): Joint " << joint_num << " does not exist." << std::endl;
	}
}

void JacobianMatrix::appendJoint(Point position, Vector zaxis)
{
	positions_.push_back(position);
	zaxes_.push_back(zaxis);
	n_joints++;
	data_.resize(6*n_joints, 0.); //adds extra space to data_
}

void JacobianMatrix::populate()
{
	//~ std::cout << "[";
	//~ for (size_t j = 0; j < data_.size(); j++)
		//~ std::cout << data_[j] << ",";
	//~ std::cout << "]\n";
	for (int i = 0; i < n_joints; i++)
	{
		Vector shift = (Vector)positions_[n_joints] - (Vector)positions_[i];
		//~ Vector shift = (Vector)positions_[i];
		Vector Jn = zaxes_[i].cross(shift);
		
		data_[i*6]   = Jn.x;
		data_[i*6+1] = Jn.y;
		data_[i*6+2] = Jn.z;
		data_[i*6+3] = zaxes_[i].x;		
		data_[i*6+4] = zaxes_[i].y;		
		data_[i*6+5] = zaxes_[i].z;		
	}
	//~ std::cout << "(";
	//~ for (size_t j = 0; j < data_.size(); j++)
		//~ std::cout << data_[j] << ",";
	//~ std::cout << ")\n";
}


std::vector<float> JacobianMatrix::applyWrench(Vector trans, Vector rot)
{
	//~ std::cout << "Z axes" << std::endl;
	//~ for (int i = 0; i < n_joints; i++)
		//~ std::cout << "(" << zaxes_[i].x << "," << zaxes_[i].y << "," << zaxes_[i].z << ")" << std::endl;
	//~ std::cout << "Posns" << std::endl;
	//~ for (int i = 0; i < n_joints; i++)
		//~ std::cout << "(" << positions_[i].x << "," << positions_[i].y << "," << positions_[i].z << ")" << std::endl;
	float wrench[] = {trans.x, trans.y, trans.z, rot.x, rot.y, rot.z};
	//~ std::vector<float>	 wrench(wrenchvals, wrenchvals+6); 
	std::vector<float> torques(n_joints, 0.);
	for (int i = 0; i < n_joints; i++)
	{
		for (int j = 0; j < 6; j++)
		{
				//~ torques[i] += data_[n_joints*i+j]*wrench[6*j]; 
				//~ torques[i] += data_[n_joints*i+j]*wrench[j]; 
				torques[i] += data_[6*i+j]*wrench[j]; 
				//~ std::cout << "(" << i << ")[" << j << "]" << torques[i] << std::endl;
		}
	} 
	return torques;
}

JacobianMatrix JacobianMatrix::getSubMatrix(int startjoint, int endjoint)
{
	std::vector<Point> posn_subvect(&positions_[startjoint], &positions_[endjoint]);
	std::vector<Vector> zaxis_subvect(&zaxes_[startjoint], &zaxes_[endjoint]);
	return JacobianMatrix(posn_subvect, zaxis_subvect);
}

std::vector<float> JacobianMatrix::getData() const
{
	return data_;
}




