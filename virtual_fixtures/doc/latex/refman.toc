\contentsline {chapter}{\numberline {1}\discretionary {-}{}{}Class \discretionary {-}{}{}Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}\discretionary {-}{}{}Class \discretionary {-}{}{}Hierarchy}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}\discretionary {-}{}{}Class \discretionary {-}{}{}Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}\discretionary {-}{}{}Class \discretionary {-}{}{}List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}\discretionary {-}{}{}Class \discretionary {-}{}{}Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}\discretionary {-}{}{}Capsule \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{5}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}\discretionary {-}{}{}Constructor \& \discretionary {-}{}{}Destructor \discretionary {-}{}{}Documentation}{6}{subsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.2.1}\discretionary {-}{}{}Capsule}{6}{subsubsection.3.1.2.1}
\contentsline {subsection}{\numberline {3.1.3}\discretionary {-}{}{}Member \discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{6}{subsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.3.1}\discretionary {-}{}{}Point\discretionary {-}{}{}In\discretionary {-}{}{}Capsule}{6}{subsubsection.3.1.3.1}
\contentsline {subsubsection}{\numberline {3.1.3.2}\discretionary {-}{}{}Point\discretionary {-}{}{}In\discretionary {-}{}{}Cylinder}{6}{subsubsection.3.1.3.2}
\contentsline {subsubsection}{\numberline {3.1.3.3}update\discretionary {-}{}{}Position}{7}{subsubsection.3.1.3.3}
\contentsline {section}{\numberline {3.2}\discretionary {-}{}{}Collision\discretionary {-}{}{}Sheath \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{7}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{8}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}\discretionary {-}{}{}Constructor \& \discretionary {-}{}{}Destructor \discretionary {-}{}{}Documentation}{8}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}\discretionary {-}{}{}Collision\discretionary {-}{}{}Sheath}{8}{subsubsection.3.2.2.1}
\contentsline {subsection}{\numberline {3.2.3}\discretionary {-}{}{}Member \discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{8}{subsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.3.1}assign\discretionary {-}{}{}Joints}{8}{subsubsection.3.2.3.1}
\contentsline {subsubsection}{\numberline {3.2.3.2}create\discretionary {-}{}{}Bounding\discretionary {-}{}{}Marker}{8}{subsubsection.3.2.3.2}
\contentsline {subsubsection}{\numberline {3.2.3.3}create\discretionary {-}{}{}R\discretionary {-}{}{}Viz\discretionary {-}{}{}Markers}{9}{subsubsection.3.2.3.3}
\contentsline {subsubsection}{\numberline {3.2.3.4}get\discretionary {-}{}{}Joint\discretionary {-}{}{}Position}{9}{subsubsection.3.2.3.4}
\contentsline {subsubsection}{\numberline {3.2.3.5}update\discretionary {-}{}{}Joint\discretionary {-}{}{}Angles}{10}{subsubsection.3.2.3.5}
\contentsline {section}{\numberline {3.3}\discretionary {-}{}{}Kraft\discretionary {-}{}{}Bumpers \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{10}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{10}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}\discretionary {-}{}{}Kraft\discretionary {-}{}{}Collision\discretionary {-}{}{}Sheath \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{10}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{11}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}\discretionary {-}{}{}Constructor \& \discretionary {-}{}{}Destructor \discretionary {-}{}{}Documentation}{11}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}\discretionary {-}{}{}Kraft\discretionary {-}{}{}Collision\discretionary {-}{}{}Sheath}{11}{subsubsection.3.4.2.1}
\contentsline {section}{\numberline {3.5}\discretionary {-}{}{}Line \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{11}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{12}{subsection.3.5.1}
\contentsline {section}{\numberline {3.6}\discretionary {-}{}{}Point \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{12}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{13}{subsection.3.6.1}
\contentsline {section}{\numberline {3.7}\discretionary {-}{}{}Quaternion \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{13}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{13}{subsection.3.7.1}
\contentsline {section}{\numberline {3.8}\discretionary {-}{}{}Transform\discretionary {-}{}{}Matrix \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{13}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{14}{subsection.3.8.1}
\contentsline {section}{\numberline {3.9}\discretionary {-}{}{}Vector \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{14}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{15}{subsection.3.9.1}
