#ifndef CAPSULE_H
#define CAPSULE_H

#include <cmath>
#include <cstddef>

#include <visualization_msgs/Marker.h>

#include "alex_technique/Types.h"

///A class based around the computer graphics concept of capsules.
/**Used for fast detection of collisions.
 * Much of the collision code is based on algorithms in
 * Ericson, C., 2005. Real-Time Collision Detection, San Francisco, CA, USA:
 *  Morgan Kaufmann Pubishers, Elsevier. pp. 113
 */
class Capsule
{
	public:
		Capsule(const Point start = Point(), const Point end = Point(), float radius = 0.);
		
		void updatePosition(const Point start, const Point end);
		
		bool PointInCylinder(const Point pt);
		bool PointInCapsule(const Point pt, Vector* return_normal, Point* closest_point = NULL, float* penetration = NULL, float offset = 0. );
		
		visualization_msgs::Marker createRVizMarker(int id, const std::string& base_frame, const std::string& ns, char colour = 'k', float alpha = 1);
		
		bool intersects(Capsule other_capsule, float* dist_along_this, float* dist_along_other, Point* point_this, Point* point_other);
		bool intersects(Capsule other_capsule);
		
		Point maxPoint(bool compare = false, Point compare_pt = Point());
		Point minPoint(bool compare = false, Point compare_pt = Point()); 
		
		Point getStart();
		Point getEnd();
		Line getLine();
		float getRadius();
		
	protected:
		Line line_;
		float radius_;
		float radius_sq_;
		
	
};
		
	
	
#endif
