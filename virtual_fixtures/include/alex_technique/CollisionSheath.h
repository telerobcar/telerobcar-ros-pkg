#ifndef COLLISIONSHEATH_H
#define COLLISIONSHEATH_H

#include "alex_technique/Types.h"
#include "alex_technique/Capsule.h"

#include "visualization_msgs/MarkerArray.h"
#include "visualization_msgs/Marker.h"

#include <vector>
#include <iostream>

///A class which hold a Collision Sheath for one robot manipulator.
///A Collision Sheath is a series of linked capsules which 
///encapsulate the links of a robotic manipulator. 
///Collisions between sheaths produce a reaction force to both
///of the manipulators, which can then be fed back to an operator
///or force control system.

struct CollisionData{
	CollisionData(bool c, int l, float d, Vector v, void* s)
		: collision(c), link(l), dist(d), vect(v), sheath_id(s){}
	bool collision;
	int link;
	float dist;
	Vector vect;
	void* sheath_id;
};

class CollisionSheath
{
	public:
		CollisionSheath(Pose base_pose = Pose(), std::vector<TransformMatrix> joints = std::vector<TransformMatrix>(), float radius = 0);
		
		void assignJoints(Pose base_pose, std::vector<TransformMatrix> joints, float radius);
		void setOffsets(std::vector<float> offsets);
		
		void updateJointAngles(std::vector<float> angles);
		Point getJointPosition(int joint_num);
		
		bool detectCollision(CollisionSheath& other_sheath, int* link_this = NULL, int* link_other = NULL, float* dist_along_this = NULL, float* dist_along_other = NULL, Point* point_along_this = NULL, Point* point_along_other = NULL);
		std::vector<float> calculateTorques();
		
		visualization_msgs::MarkerArray createRVizMarkers(int id, const std::string& base_frame, const std::string& ns, char colour = 'k', float alpha = 1, bool show_bounding_box = false, bool show_force_arrows = false);
		visualization_msgs::MarkerArray createRVizMarkers(int robot_number, float alpha = 1, bool show_bounding_box = false, bool show_force_arrows = false);
		
		visualization_msgs::Marker createBoundingMarker(int id, const std::string& base_frame, const std::string& ns, char colour = 'k', float alpha = 1);
		std::vector<visualization_msgs::Marker> getForceArrows(int id, const std::string& base_frame, const std::string& ns, bool reset = true);

		std::vector<float> getTorques(float stiffness, float min_torque, float max_torque);
		
		int nJoints() const { return n_joints_; }
		int nLinks() const { return n_capsules_; }
		float getRadius() const { return radius_; }
		Point maxPoint() const { return max_point_; }
		Point minPoint() const { return min_point_; }
		
	protected:
		std::vector<Capsule> capsules_;
		Point max_point_, min_point_;
		int n_joints_;
		int n_capsules_;
		void* sheath_id_;
		
		void addCollision(bool collision, CollisionData new_collision);//used to update info in other sheath
		void addForceArrow(Point start, Vector direction);
		
	private:
		TransformMatrix base_pose_transform_;
		std::vector<TransformMatrix> joint_transforms_;
		std::vector<TransformMatrix> running_transforms_;
		std::vector<Point> joint_posns_;
		float radius_;
		
		std::vector<float> joint_angle_offsets_;
		std::vector<visualization_msgs::Marker> force_arrows_; //for visualisation
		
		std::vector<CollisionData> collisions_; 
		JacobianMatrix jacobian_;
	
};
		
class KraftCollisionSheath : public CollisionSheath
{
	public:
		KraftCollisionSheath(Point base_posn = Point(0,0,0), float rot_x = 0, float rot_y = 0, float rot_z = 0);
		KraftCollisionSheath(Pose base_pose = Pose(0,0,0));
	
};


#endif
