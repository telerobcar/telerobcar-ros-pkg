#ifndef TYPES_H
#define TYPES_H

#include <cmath>
#include <vector>
#include <iostream>//debug only


// ----------------------INLINE FUNCTIONS------------------------------
//---------used for imprecise comparisons (in reality is never zero)
#define EPSILON 0.0001
inline bool isEqual(float a, float b){ return std::abs(a-b) <= EPSILON; }
inline bool isZero(float a){ return std::abs(a) <= EPSILON; }

//------ Clamp function
inline float clamp(float n, float min, float max) { return n < min ? min : ( n > max ? max : n ); }

///A standalone class which describes a point
class Point
{
	public: 
		Point(float x_ = 0, float y_ = 0, float z_ = 0);
			
		float x;
		float y;
		float z;
		
		Point operator+(const Point pt) const;
		Point operator-(const Point pt) const;
		Point operator*(const Point pt) const;
		Point operator*(float scalar) const;
		Point operator/(float scalar) const;
		
		float distanceSquared(const Point pt) const;
		float distanceTo(const Point pt) const;

};

///A standalone class which describes a vector. Derived from Point.
class Vector : public Point
{
	public: 
		Vector(float x_ = 0, float y_ = 0, float z_ = 0);
		Vector(const Point A, const Point B);	
		Vector(const Point pt);

		Vector operator*(const Vector vec) const;
		Vector operator-(const Vector vec) const;
		
		Vector operator*(float scalar) const;
		Vector operator-() const;
		
		float length_sq() const;
		float length() const;
		
		float distanceTo(const Point pt) const;
		
		float dot(const Vector vect) const;
		Vector cross(const Vector vect) const;
};

///A standalone class which describes a quaternion.
class Quaternion
{
	public:		
		Quaternion(float x_ = 0, float y_ = 0, float z_ = 0, float w_ = 1):x(x_), y(y_), z(z_), w(w_){}
		Quaternion(Vector vect1, Vector vect2);
		
		float x;
		float y;
		float z;
		float w;
		
		Quaternion operator*(const Quaternion total) const;
		
		void normalise();
};

///A standalone class which defines pose by point and euler angle
class Pose
{
	public:
		Pose(Point posn_ = Point(), float rot_x_ = 0, float rot_y_ = 0, float rot_z_ = 0);
		
		Point posn;
		float rot_x;
		float rot_y;
		float rot_z;
};

///A standalone class which describes a line.
class Line
{
	public:
		Line(const Point start_ = Point(), const Point end_ = Point());
		
		Point start;
		Point end;
		Vector vect;
		float length_sq; //length squared. 
		float length;
		
		Point midpoint() const;
		//float length() const;
		
		float sqDistanceTo(const Point pt) const;
		
		float closestPtLineLine(Line other_line, Point* pt_this, Point* pt_other, float* dist_along_this, float* dist_along_other) const;
		float closestPtLineLineExact(Point p1, Point q1, Point p2, Point q2, float &s, float &t, Point &c1, Point &c2);
		float closestPtLineLine(Line other_line) const;
		
};

/**@brief A standalone class which describes a Transform Matrix.
 * Can be used for both a revolute or a linear actuator. Data is stored in rows.
 */
class TransformMatrix
{
	public:
		TransformMatrix(bool identity = true);
		TransformMatrix(Point position, float rot_x = 0, float rot_y = 0, float rot_z = 0); //translation only
		TransformMatrix(float d, float theta, float a, float alpha);
		
		TransformMatrix operator*(const TransformMatrix mat) const;
		
		void setIdentity();
		
		void setTheta(float theta); //rotational actuator
		void setD(float d); // linear actuator
		
		Point getPosn() const;
		Vector getZaxis() const;
	
	private:
		float salpha_;
		float calpha_;
		float stheta_;
		float ctheta_;
		float a_;
		
		std::vector<float> data_;
};

/**@brief A standalone class which describes a Manipulator Jacobian Matrix.
 * Can be used for revolute actuators. Data is stored in rows of a n x 6 
 * manipulator jacobian - thus it is really a transposed jacobian.
 */
class JacobianMatrix
{
	public:
		JacobianMatrix(int num_joints = 0);
		JacobianMatrix(std::vector<Point> positions, std::vector<Vector> zaxes);
		
		void setNjoints(int num_joints);
		
		void updateJoint(int joint_num, Point position, Vector zaxis);
		void appendJoint(Point position, Vector zaxis);
		
		void populate();
		std::vector<float> applyWrench(Vector trans, Vector rot);
		
		JacobianMatrix getSubMatrix(int startjoint, int endjoint);
		
		std::vector<float> getData() const;
		
		int n_joints;
		
	private:
		std::vector<float> data_;
		std::vector<Vector> zaxes_;
		std::vector<Point> positions_;
};

#endif

