#include "ros/ros.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Vector3.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "kraft_virtual_fixture/RectSelect.h"
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"

#include "wall_defines.h"

#define WALL_THICKNESS 0.05

//set up topic to post end effector position
ros::Publisher pub;
ros::Publisher vis_pub;
ros::Publisher pen_pub;
geometry_msgs::Point select_max, select_min;
geometry_msgs::Point centre, inner_max, inner_min;
std_msgs::Float32 penetration;
std_msgs::Bool is_activated;

/*functions to draw objects*/
void drawObjects(geometry_msgs::Point point)
{
	visualization_msgs::MarkerArray marker_arr;
	visualization_msgs::Marker marker, marker2, marker3;

	marker.header.frame_id = marker2.header.frame_id = marker3.header.frame_id = "base_link";
	marker.header.stamp = marker2.header.stamp = ros::Time();

	//draw centre point
	marker.id = 0;
	marker.type = visualization_msgs::Marker::SPHERE; 
	marker.ns = "objects";
	marker.action = visualization_msgs::Marker::ADD;
	marker.pose.position.x = point.x;
	marker.pose.position.y = point.y;
	marker.pose.position.z = point.z;
	marker.scale.x = 0.1;
	marker.scale.y = 0.1;
	marker.scale.z = 0.1;
	marker.color.a = 1.0;
	marker.color.r = 1.0;
	marker.color.g = 1.0;

	//draw inner box
	marker2.id = 1;
	marker2.type = visualization_msgs::Marker::CUBE;
	marker2.ns = "objects";
	marker2.action = visualization_msgs::Marker::ADD;
	marker2.pose.position.x = point.x;
	marker2.pose.position.y = point.y;
	marker2.pose.position.z = point.z;
	      
	marker2.scale.x = (select_max.x-select_min.x) - (WALL_THICKNESS*2);
	marker2.scale.y = (select_max.y-select_min.y) - (WALL_THICKNESS*2);
	marker2.scale.z = (select_max.z-select_min.z) - (WALL_THICKNESS*2);
	      
	marker2.color.a = 0.3;
	marker2.color.r = 0.2;
	marker2.color.g = 0.2;
	marker2.color.b = 0.8;
	
	//draw outer box
	marker3.id = 2;
	marker3.type = visualization_msgs::Marker::CUBE;
	marker3.ns = "objects";
	marker3.action = visualization_msgs::Marker::ADD;
	marker3.pose.position.x = point.x;
	marker3.pose.position.y = point.y;
	marker3.pose.position.z = point.z;
	     
	marker3.scale.x = select_max.x-select_min.x;
	marker3.scale.y = select_max.y-select_min.y;
	marker3.scale.z = select_max.z-select_min.z;
	     
	marker3.color.a = 0.4;
	
	//change colour if activated
	if (is_activated.data){
	    marker3.color.r = 0.9; 
	    marker3.color.g = 0.2;
	    marker3.color.b = 0.2;
	  }else{
	    marker3.color.r = 0.5;
	    marker3.color.g = 0.5;
	    marker3.color.b = 0.5;
	  }
	
	marker_arr.markers.push_back(marker);
	marker_arr.markers.push_back(marker2);
	marker_arr.markers.push_back(marker3);
	
	vis_pub.publish( marker_arr );
}

//a very simplistic distance from end effector to plane
float calculatePenetration(geometry_msgs::Point ee)
{
	int closest_wall[3];
	
	//find nearest side of the box in each direction
	closest_wall[0] = ((select_max.x - ee.x) < (ee.x - select_min.x)) ? MAX_X : MIN_X;
	closest_wall[1] = ((select_max.y - ee.y) < (ee.y - select_min.y)) ? MAX_Y : MIN_Y;
	closest_wall[2] = ((select_max.z - ee.z) < (ee.z - select_min.z)) ? MAX_Z : MIN_Z;
	
	/* find the closest side from the 3 closest walls, by calculating the distance 
		from the closest point on each one and taking the smallest */
		
	geometry_msgs::Point nearest_pt[3];
	
	//fill all nearest points with the end effector position
        for (int i=0; i<3; i++){
        	nearest_pt[i].x = ee.x;
        	nearest_pt[i].y = ee.y;
        	nearest_pt[i].z = ee.z;
        }
	
	//change the points to project them onto each of the candidate sides
	nearest_pt[0].x = (closest_wall[0] == MAX_X) ? select_max.x : select_min.x;
	nearest_pt[1].y = (closest_wall[1] == MAX_Y) ? select_max.y : select_min.y;
	nearest_pt[2].z = (closest_wall[2] == MAX_Z) ? select_max.z : select_min.z;
		
	//calculate distance from the 3 candidate points to the end effector position
	float distances[3];
	float shortest_distance = 9999;	//note: I know this is not good practice, but here would correspond to 10km distance
	for (int j=0; j < 3; j++){
		float xd = nearest_pt[j].x - ee.x;
		float yd = nearest_pt[j].y - ee.y;
		float zd = nearest_pt[j].z - ee.z;
		distances[j] = sqrt( (xd*xd) + (yd*yd) + (zd*zd) );
		
		if (distances[j] < shortest_distance)
			shortest_distance = distances[j];
	}
	
	return shortest_distance/WALL_THICKNESS;  //value scaled between 0 and 1
}


/*Callback functions*/
void changedSelectCallback(const kraft_virtual_fixture::RectSelect::ConstPtr& msg)
{
	//update selection
	select_max.x = msg->max.x;
	select_max.y = msg->max.y;
	select_max.z = msg->max.z;
	
	select_min.x = msg->min.x;
	select_min.y = msg->min.y;
	select_min.z = msg->min.z;
	
	centre.x  = msg->centre.x;
	centre.y  = msg->centre.y;
	centre.z  = msg->centre.z;
	
	inner_max.x = select_max.x - WALL_THICKNESS;
	inner_max.y = select_max.y - WALL_THICKNESS;
	inner_max.z = select_max.z - WALL_THICKNESS;
	inner_min.x = select_min.x + WALL_THICKNESS;
	inner_min.y = select_min.y + WALL_THICKNESS;
	inner_min.z = select_min.z + WALL_THICKNESS;
	ROS_INFO("outer_max: [%f,%f,%f]", select_max.x, select_max.y, select_max.z);
	ROS_INFO("inner_max: [%f,%f,%f]", inner_max.x, inner_max.y, inner_max.z);
	ROS_INFO("outer_min: [%f,%f,%f]", select_min.x, select_min.y, select_min.z);
	ROS_INFO("inner_min: [%f,%f,%f]", inner_min.x, inner_min.y, inner_min.z);
	
	drawObjects(centre);
}

void newPosnCallback(const geometry_msgs::Point::ConstPtr& msg)
{
	geometry_msgs::Point ee;
	ee.x = msg->x;
	ee.y = msg->y;
	ee.z = msg->z;
	
	//activate the box if end effector is within selection box, else deactivate
	if (ee.x < select_max.x && ee.y < select_max.y && ee.z < select_max.z &&
	    ee.x > select_min.x && ee.y > select_min.y && ee.z > select_min.z) {
	
	    	//deactivate the box if end effector is inside inner box
	  	if( ee.x < inner_max.x && ee.y < inner_max.y && ee.z < inner_max.z &&
	      	    ee.x > inner_min.x && ee.y > inner_min.y && ee.z > inner_min.z ) {
	    		if (is_activated.data == true){
	    			is_activated.data = false;
				pub.publish(is_activated);
				drawObjects(centre);
				
				//send -1 as a control signal for being inside inner box
				penetration.data = -1.0;		
				pen_pub.publish(penetration);
			}
	   	} else {
			//activate box if between outer and inner selection box
	  		if (is_activated.data == false){
	    			is_activated.data = true;
	    			pub.publish(is_activated);
				drawObjects(centre);
	    		}
			penetration.data = calculatePenetration(ee);
			pen_pub.publish(penetration);
			
		}
	} else {
		//deactivate box if outside selection box
	    	if (is_activated.data == true){
	    		is_activated.data = false;
			pub.publish(is_activated);
			drawObjects(centre);
			
			penetration.data = 0.0;
			pen_pub.publish(penetration);
	    	}
	}
	
     // ROS_INFO("outer, inner, ee: [%f/%f,%f/%f,%f/%f], [%f/%f,%f/%f,%f/%f], [%f,%f,%f]", 
     // 						select_min.x, select_max.x, select_min.y, select_max.y, select_min.z, select_max.z,
     // 						inner_min.x, inner_max.x, inner_min.y, inner_max.y, inner_min.z, inner_max.z,
     // 						ee.x, ee.y, ee.z);
	
}

int main(int argc, char **argv)
{    
	//initialise node
  	ros::init(argc, argv, "activate_selection");
  	ros::NodeHandle n;
  
  	//box starts unactivated
	is_activated.data = false;
	
	pub = n.advertise<std_msgs::Bool>("box_activated", 1);
	vis_pub = n.advertise<visualization_msgs::MarkerArray>( "virtual_selection", 0 );
	pen_pub = n.advertise<std_msgs::Float32>( "box_penetration", 0 );
	ros::Subscriber sub1 = n.subscribe("end_effector_posn", 1, newPosnCallback, ros::TransportHints().udp());
	ros::Subscriber sub2 = n.subscribe("selection_max_min", 10, changedSelectCallback);
	
	ros::spin();
	return 0;
}
