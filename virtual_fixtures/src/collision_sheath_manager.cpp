#include <iostream>
#include <vector>
#include <sstream>
#include <cstdlib> // for rand

#include <boost/thread.hpp> //thread and mutex support
#include <boost/ptr_container/ptr_vector.hpp> //for pointer vectors to mutex
#include <boost/asio.hpp> //for udp
#include <boost/date_time.hpp> //for thread time restrictions

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include "alex_technique/CollisionSheath.h"

#define MAX_BUFFER_SIZE 1024

using boost::asio::ip::udp;

class UDPListeners 
{
    public:
	UDPListeners(std::vector<int> ports)
	{	    	    
	    n_listeners_ = (int)ports.size();    
	    
	    for (int i = 0; i < n_listeners_; i++)
	    {
		std::cout << "Creating Listener " << i << std::endl;
		
		//Create a JointStates to share with each thread
		mutexes_.push_back(new boost::mutex());
		joint_states_.push_back(new sensor_msgs::JointState);	
		
		thread_ptrs_.push_back(listener_threads_.create_thread(
			    boost::bind(&UDPListeners::listen_function, this, ports[i], 
			    boost::ref(joint_states_[i]), boost::ref(mutexes_[i]))));
	    }
	}
	~UDPListeners()
	{
	    //ask all threads to close
	    listener_threads_.interrupt_all();	
	    
	    std::cout << "threads interrupted" << std::endl;
	    
	    //wait until they actually do (with timeout)
	    boost::posix_time::seconds timeout(1); 

	    for (int i = 0; i < n_listeners_; i++)
	    {
		thread_ptrs_[i]->timed_join(timeout);
	    }
	    listener_threads_.join_all();
	}
	
	void print_states()
	{
	    boost::posix_time::seconds workTime(0.5);
	    
	    for (int i = 0; i < n_listeners_; i++)
	    {
		sensor_msgs::JointState read_state;
		
		//perform elemental action of reading joint state
		mutexes_[i].lock();
		read_state = joint_states_[i];
		mutexes_[i].unlock();
		
		//Print out the joint states to screen
		int njoints = (int)read_state.name.size();
		std::cout << "[";
		for (int j = 0; j < njoints; j++)
		{
		    std::cout << read_state.name[j] << ",";
		}
		std::cout << "]" << std::endl;
		std::cout << "(";
		for (int j = 0; j < njoints; j++)
		{
		    std::cout << read_state.position[j] << ",";
		}
		std::cout << ")" << std::endl;
		std::cout << std::endl;
	    }
		
	    boost::this_thread::sleep(workTime);
	}
	
	std::vector<sensor_msgs::JointState> get_states()
	{
	    std::vector<sensor_msgs::JointState> states;
	    for (int i = 0; i < n_listeners_; i++)
	    {
		sensor_msgs::JointState read_state;
		
		//perform elemental action of reading joint state
		mutexes_[i].lock();
		read_state = joint_states_[i];
		mutexes_[i].unlock();
		
		states.push_back(read_state);
	    }
	    return states;
	}
	
	int nListeners(){return n_listeners_;}
	
    private:
	int n_listeners_;
	
	//thread and shared data objects
	boost::thread_group listener_threads_;  
	std::vector<boost::thread*> thread_ptrs_;
	boost::ptr_vector<sensor_msgs::JointState> joint_states_;
	boost::ptr_vector<boost::mutex> mutexes_;
	
	/**One instance of listen_function runs on each thread, 
	 * listening for JointState Messages a port and saving them 
	 * in shared memory for the main thread to use
	 */
	void listen_function(int port_num, sensor_msgs::JointState& jointstateref, boost::mutex& mutexref)
	{
	    std::cout << "Listening for JointState messages on Port: " << port_num << std::endl;
	    //Setup a UDP socket
	    try
	    {
		boost::asio::io_service io_service;
		udp::socket socket(io_service, udp::endpoint(udp::v4(), port_num));
		
		for (;;)
		{
		    try
		    {		
			
			boost::this_thread::interruption_point(); //allow thread to be killed
			
			boost::array<uint8_t,MAX_BUFFER_SIZE> recv_buf;
			udp::endpoint remote_endpoint;
			boost::system::error_code error;
		
			//Sit and wait for a new message to come in
			socket.receive_from(boost::asio::buffer(recv_buf),remote_endpoint, 0, error);

			//deserialize the data into a JointState message
			sensor_msgs::JointState new_joint_state;
			ros::serialization::IStream stream(&recv_buf[0], MAX_BUFFER_SIZE);
			ros::serialization::Serializer<sensor_msgs::JointState>::read(stream, new_joint_state);
			
			//perform elemental action of writing new joint state
			mutexref.lock();
			jointstateref = new_joint_state;
			mutexref.unlock();
			
		    }
		    catch(boost::thread_interrupted&)
		    {
			std::cout << "Port Number: " << port_num << " thread interrupted" << std::endl;
			break;
		    }
		}
	    }
	    catch (std::exception& e)
	    {
		ROS_ERROR_STREAM(e.what());
	    }
	    
	    std::cout << "Port Number: " << port_num << " closing" << std::endl;
	}
	
};	

class UDPTalker
{
    public:
	UDPTalker(int rate_hz, std::vector<std::string> ips, std::vector<int> ports, std::vector< std::vector<std::string> > jointnames)
	{	    	    
	    if (ports.size() != ips.size())
	    {
		ROS_ERROR("ERROR port and ip vectors are not the same length");
		return;
	    }
		
	    n_listeners_ = (int)ports.size();
	    
	    jointnames_ = jointnames;
	    
            //Create a JointStates for each robot
	    joint_states_ = new std::vector<sensor_msgs::JointState>;
	    for (int i = 0; i < n_listeners_; i++)
	    {		
		sensor_msgs::JointState emptyjoint;
		joint_states_->push_back(emptyjoint);	
	    }
	    
	    //create a mutex to share the 
	    mutex_ = new boost::mutex();
	    std::cout << "Creating Talker Thread " << std::endl;
	    thread_ptr_ = new boost::thread(
			    boost::bind(&UDPTalker::talk_function, this, rate_hz, ips, ports, 
			    boost::ref(*joint_states_), boost::ref(*mutex_)));
	    
	}
	
	void writeTorques(std::vector< std::vector<float> > torques)
	{
	    if ((int)torques.size() != n_listeners_)
	    {
		ROS_ERROR_STREAM("ERROR (UDPTalker::writeTorques): Torques provided for " << torques.size() << " robots but " << n_listeners_ << " needed");
		return;
	    }
	    
	    std::vector<sensor_msgs::JointState> newstates;
	    for (int i = 0; i < n_listeners_; i++)
	    {
		if (torques[i].size() != jointnames_[i].size())
		{
		    ROS_WARN_STREAM("WARNING: Robot " << i << " needs " << jointnames_.size() << " joints but given " << torques[i].size());
		    newstates.push_back(sensor_msgs::JointState());
		    continue;
		}
		sensor_msgs::JointState newstate;
		newstate.name.assign(jointnames_[i].begin(), jointnames_[i].end());
		newstate.effort.assign(torques[i].begin(), torques[i].end());
		newstates.push_back(newstate);
		
	    }
	    //perform elemental action of writing new joint states
	    mutex_->lock();
	    *joint_states_ = newstates;
	    mutex_->unlock();
	}
	
	int nListeners(){return n_listeners_;}
	
    private:
	int n_listeners_;
	
	//thread and shared data objects
	boost::thread* thread_ptr_;
	std::vector<sensor_msgs::JointState>* joint_states_;
	boost::mutex* mutex_;
	
	std::vector< std::vector<std::string> > jointnames_;
	
	/**One instance of the talk_function runs in a separate thread, 
	 * sending JointState Messages to remote ports port and saving them 
	 * in shared memory for the main thread to use
	 */
	void talk_function(int rate_hz, std::vector<std::string> ips, std::vector<int> ports, std::vector<sensor_msgs::JointState>& jointstatesref, boost::mutex& mutexref)
	{
	    //calculate sleep time from desired send rate
	    boost::posix_time::time_duration sleeptime;
	    sleeptime = boost::posix_time::millisec(1000./(float)rate_hz);
	    
	    //make the ip addresses into boost and open sockets
	    //start the sockets
	    boost::asio::io_service io_service;
	    
	    std::vector<udp::endpoint> targetEndpoints;
	    boost::ptr_vector<udp::socket> sockets;
	    for (int i = 0; i < n_listeners_; i++)
	    {    
		//make an ip object, endpoint and socket for each robot
		boost::asio::ip::address_v4 newIP;
		newIP = boost::asio::ip::address_v4::from_string(ips[i]);

		udp::socket* newSocket;
		newSocket = new udp::socket(io_service);
		sockets.push_back(newSocket);
		sockets[i].open(udp::v4());
		
		std::ostringstream portnum;
		portnum << ports[i];
		udp::resolver resolver(io_service);
		udp::resolver::query query(ips[i].c_str(), portnum.str());
		udp::endpoint newEndpoint = *resolver.resolve(query);
		targetEndpoints.push_back(newEndpoint);
		
	    }
	
	    
	    for (;;)
	    {
		//get the time now
		boost::posix_time::ptime start = boost::posix_time::microsec_clock::local_time();
		
		//perform elemental action of reading new joint states
		std::vector<sensor_msgs::JointState> joints_states;
		mutexref.lock();
		joints_states = jointstatesref;
		mutexref.unlock();
		
		//serialise jointstate for each robot and send
		for (int i = 0; i < n_listeners_; i++)
		{
		    boost::array<uint8_t,MAX_BUFFER_SIZE> send_buf;
		    send_buf.assign(0);
		    ros::serialization::OStream stream(&send_buf[0], MAX_BUFFER_SIZE);
		    ros::serialization::Serializer<sensor_msgs::JointState>::write(stream, joints_states[i]);

		    sockets[i].send_to(boost::asio::buffer(send_buf), targetEndpoints[i]);
		}
		
		//wait for the required time (subtracting time taken to send messages)
		boost::posix_time::ptime end = boost::posix_time::microsec_clock::local_time();
		boost::this_thread::sleep(sleeptime - (end-start));
	    }
	}
};

class CollisionSheathManager
{
    public:
	CollisionSheathManager(ros::NodeHandle* n, int rate_hz, std::vector<std::string> ips, std::vector<int> local_ports, std::vector<int> remote_ports, std::vector<Pose> start_poses, std::vector< std::vector<std::string> > jointnames)
	{
	    if (local_ports.size() != ips.size() && remote_ports.size() != ips.size() )
		ROS_ERROR("ERROR port and ip vectors are not the same length");
		
	    n_robots_ = (int)local_ports.size();
	    rate_hz_ = rate_hz;
	    jointnames_ = jointnames;
	    
	    std::cout << "Robots: " << n_robots_ << std::endl;
	    
	    listeners_ = new UDPListeners(local_ports);
	    talker_ = new UDPTalker(rate_hz_, ips, remote_ports, jointnames);
	    
	    jointstatepub_ = n->advertise<sensor_msgs::JointState>( "joint_states", 0 );
	    
	    //add the sheaths - later i will make this less kraft specific
	    //(i.e. by including CollisionSheath instead of KraftCollisionSheath)
	    //but right now I just want the thing to work (Jan 2014)
	    for (int i = 0; i < n_robots_; i++)
	    {
		sheaths_.push_back(new KraftCollisionSheath(start_poses[i]));
	    }
	}
	
	void run_once()
	{
	    //~ listeners_->print_states();
	    states_ = listeners_->get_states();
	    for (int i = 0; i < n_robots_; i++)
	    {	    
		std::vector<float> new_positions = sort_joints_positions(i, states_[i]);
		
		if ((int)new_positions.size() != sheaths_[i].nJoints())
		{
		    std::stringstream warnname;
		    warnname << "robot_" << i;
		    ROS_WARN_STREAM_THROTTLE_NAMED(1,warnname.str().c_str(),"Robot " << i << " Wrong Number of Joints Received: Needed " << sheaths_[i].nJoints() << " but got " << new_positions.size());
		    //~ ROS_WARN_STREAM_THROTTLE(1,"Robot " << i << " Wrong Number of Joints Received: Needed " << sheaths_[i].nJoints() << " but got " << new_positions.size());
		    continue;
		}
		
		sheaths_[i].updateJointAngles(new_positions);
	    } 

	    //detect collisions between all robots
	    std::vector< std::vector<float> > torques; //maybe include a bool to enable force reflection or not 
	    for (int i = 0; i < n_robots_; i++)
	    {
		for (int j = i; j < n_robots_; j++) //don't check the ones already checked
		{
		    if (i != j) //no self collision checking
		    {
			if (sheaths_[i].detectCollision(sheaths_[j]))
			{
			    //~ ROS_INFO_STREAM("Collision Detected between Robot " << i << " and Robot " << j);
			}
		    }
		}
				
		//~ std::cout << std::endl << "Robot " << i << std::endl;
		
		std::vector<float> t = sheaths_[i].getTorques( -1e5, -5, 5 );
		
		//~ std::cout << "postclamp [";
		//~ for (size_t j = 0; j < t.size(); j++)
		    //~ std::cout << t[j] << ",";
		//~ std::cout << "]" << std::endl;
		
		//filter out all but one joint for debug
		int testjoint = 0;
		for (int k = 0; k < (int)t.size(); k++)
		    if (k != testjoint)
			t[k] = 0.;
		torques.push_back(t);
	    }
	    talker_->writeTorques(torques);
	}
	
	visualization_msgs::MarkerArray create_markers()
	{
	    visualization_msgs::MarkerArray allmarkers;
	    for (int i = 0; i < n_robots_; i++)
	    {
		visualization_msgs::MarkerArray robotmarkers;
		robotmarkers = sheaths_[i].createRVizMarkers(i, 0.5, false, true);
		for (std::size_t j = 0; j < robotmarkers.markers.size(); j++)
		    allmarkers.markers.push_back(robotmarkers.markers[j]);
	    }
	    return allmarkers;	
	}
	
	void publish_states_to_ros()
	{
	    for (int i = 0; i < n_robots_; i++)
	    {
		states_[i].header.stamp = ros::Time::now();
		jointstatepub_.publish(states_[i]);
	    }
	}
	
    private:
	UDPListeners* listeners_;
	UDPTalker* talker_;
	boost::ptr_vector<KraftCollisionSheath> sheaths_;
	
	std::vector< std::vector<std::string> > jointnames_;
	std::vector<float> torque_limit_min_;
	std::vector<float> torque_limit_max_;
	
	std::vector<sensor_msgs::JointState> states_;
	ros::Publisher jointstatepub_;
	
	int n_robots_;
	int rate_hz_;
	
	//sorts the joints by name.
	std::vector<float> sort_joints_positions(int robot_num, sensor_msgs::JointState jointstate)
	{
	    std::vector<float> sorted_positions;
	    std::vector<std::string> names = jointnames_[robot_num];
	
	    if (jointstate.name.size() == jointstate.position.size())
	    {
		for (std::size_t i = 0; i < names.size(); i++)
		{
		    for (std::size_t j = 0; j < jointstate.name.size(); j++)
		    {
			if (jointstate.name[j] == names[i])
			    sorted_positions.push_back(jointstate.position[j]);
		    }
		}
	    }
	    else {
		ROS_ERROR("ERROR (CollisionSheathManager::sort_joints_positions): Names and Positions not same size in JointState");
	    }
	    	 
	    return sorted_positions;
	}
};

int main(int argc, char* argv[])
{
    //initialise node
    ros::init(argc, argv, "collision_sheath_manager");
    ros::NodeHandle n;
        
    std::string ip[] = {"138.100.76.191", "138.100.76.191"};
    int local_ports[] = {8001, 8002};
    //~ int remote_ports[] = {8051, 8052};
    int remote_ports[] = {8052, 8051};

    std::vector<int> localportsvect;
    std::vector<int> remoteportsvect;
    std::vector<std::string> ipvect;
    for (int i = 0; i < sizeof(local_ports)/sizeof(int); i++)
    {
	ipvect.push_back(std::string(ip[i]));
	localportsvect.push_back(local_ports[i]);
	remoteportsvect.push_back(remote_ports[i]);
    }
    
    std::vector<Pose> startposesvect;
    //~ startposesvect.push_back(Pose(Point(0,0,0.801),0, 0, M_PI_2));
    //~ startposesvect.push_back(Pose(Point(1,0,0.801),0, 0, M_PI_2));
    startposesvect.push_back(Pose(Point(0,0.3,0.501),0, 0, M_PI_2));
    startposesvect.push_back(Pose(Point(0,0,0.801),0, 0, M_PI_2));

    const char* leftnames[] = {"left_SA","left_SE","left_linkage_tr","left_WP","left_WY","left_WR"};
    const char* rightnames[] = {"right_SA","right_SE","right_linkage_tr","right_WP","right_WY","right_WR"};
    std::vector< std::vector<std::string> > jointnames;
    std::vector<std::string> leftjn(leftnames, leftnames+6);
    jointnames.push_back(leftjn);
    std::vector<std::string> rightjn(rightnames, rightnames+6);
    jointnames.push_back(rightjn);
 
    CollisionSheathManager manager(&n,1000, ipvect, localportsvect, remoteportsvect, startposesvect, jointnames);//send at 10Hz (for now)    
    ros::Publisher vis_pub = n.advertise<visualization_msgs::MarkerArray>( "visualization_markers", 0 );
    
    while (ros::ok())
    {
	manager.run_once();
	vis_pub.publish(manager.create_markers());
	//~ manager.publish_states_to_ros();
	//wait until ctrl-c called
    }    
    
    return 0;
}
