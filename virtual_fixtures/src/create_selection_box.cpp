#include <ros/ros.h>
#include <stdlib.h>

#include <interactive_markers/interactive_marker_server.h>
#include <interactive_markers/tools.h>

#include <geometry_msgs/Vector3.h>
#include <std_msgs/Bool.h>

#include <algorithm>

#include <virtual_fixtures/SingleBox.h>
#include <virtual_fixtures/Capsule.h>

namespace vm = visualization_msgs;

/* Class to make a 3D selection box */
class SelectorBox {
	public:
		SelectorBox( boost::shared_ptr<interactive_markers::InteractiveMarkerServer> server )
		{	
			server_ = server;
			
			min_sel_.x = -0.5;
			min_sel_.y = -1;
			min_sel_.z = -0.5;
			max_sel_.x = 0.5;
			max_sel_.y = 0;
			max_sel_.z = 0.5;
			
			//update visual items of selection
			updateBox( );
   			makeSizeHandles();
   			
   			ros::NodeHandle n;
   			pub_ = n.advertise<virtual_fixtures::Capsule>( "capsule", 0, true );
		}
		void processAxisFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback);
		vm::Marker makeBox( vm::InteractiveMarker &msg, geometry_msgs::Vector3 min_bound, geometry_msgs::Vector3 max_bound );
		vm::Marker domedEnd( bool top_nbottom );
		void updateBox();
		void makeSizeHandles( );
		void updateSizeHandles( );
		
	private:
		ros::Publisher pub_;
		geometry_msgs::Point centre_;
		
		boost::shared_ptr<interactive_markers::InteractiveMarkerServer> server_;

		geometry_msgs::Vector3 min_sel_, max_sel_;
};


//function to process when a change has been made to a handle
void SelectorBox::processAxisFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback)	
{
	ROS_INFO_STREAM( feedback->marker_name << " is now at " << feedback->pose.position.x << ", " << feedback->pose.position.y << ", " << feedback->pose.position.z );
	
	//update the internal class variables
	if ( feedback->marker_name == "min_x" ) 
	{
		min_sel_.x = feedback->pose.position.x;
		//~ float centre_y = 0.5*(max_sel_.y()-min_sel_.y());
		max_sel_.y = feedback->pose.position.y  + 0.5*(max_sel_.x-min_sel_.x) ;
		min_sel_.y = feedback->pose.position.y  - 0.5*(max_sel_.x-min_sel_.x) ;
	}
	if ( feedback->marker_name == "max_x" ) 
	{
		max_sel_.x = feedback->pose.position.x ;
		//~ float centre_y = 0.5*(max_sel_.y()-min_sel_.y());
		max_sel_.y = feedback->pose.position.y + 0.5*(max_sel_.x-min_sel_.x) ;
		min_sel_.y = feedback->pose.position.y - 0.5*(max_sel_.x-min_sel_.x) ;
	}
	if ( feedback->marker_name == "min_y" ) 
	{
		min_sel_.y = feedback->pose.position.y ;
		max_sel_.y = feedback->pose.position.y + (max_sel_.x-min_sel_.x);
	}
	if ( feedback->marker_name == "max_y" ) 
	{
		max_sel_.y = feedback->pose.position.y ;
		min_sel_.y = feedback->pose.position.y - (max_sel_.x-min_sel_.x);
	}
	if ( feedback->marker_name == "min_z" ) min_sel_.z = feedback->pose.position.z;
	if ( feedback->marker_name == "max_z" ) max_sel_.z = feedback->pose.position.z;
	
	//update even if nothing has changed
	updateBox( );
    updateSizeHandles();
    
    //send a capsule to test out the capsule interface
    virtual_fixtures::Capsule capsule;
    capsule.start.x = 0.5*(max_sel_.x+min_sel_.x);
    capsule.start.y = 0.5*(max_sel_.y+min_sel_.y);
    capsule.start.z = min_sel_.z;
    capsule.end.x = 0.5*(max_sel_.x+min_sel_.x);
    capsule.end.y = 0.5*(max_sel_.y+min_sel_.y);
    capsule.end.z = max_sel_.z;
    capsule.radius.data = (max_sel_.x-min_sel_.x)/2;
    pub_.publish(capsule);
	
    server_->applyChanges();
}


//function to create the selection box, called each time to remake it when it is changed
vm::Marker SelectorBox::makeBox( vm::InteractiveMarker &msg, geometry_msgs::Vector3 min_bound, geometry_msgs::Vector3 max_bound ) 
{
	  vm::Marker marker;

	  marker.type = vm::Marker::CYLINDER;
	  marker.scale.x = max_bound.x - min_bound.x;
	  marker.scale.y = max_bound.x - min_bound.x;
	  marker.scale.z = max_bound.z - min_bound.z;
	  marker.pose.position.x = 0.5 * ( max_bound.x + min_bound.x );
	  marker.pose.position.y = 0.5 * ( max_bound.y + min_bound.y );
	  marker.pose.position.z = 0.5 * ( max_bound.z + min_bound.z );
	  
	  marker.color.r = 0.5;
	  marker.color.g = 0.5;
	  marker.color.b = 0.5;
	  marker.color.a = 0.9;
	  
	  centre_.x = marker.pose.position.x;
	  centre_.y = marker.pose.position.y;
	  centre_.z = marker.pose.position.z;

	  return marker;
}

vm::Marker SelectorBox::domedEnd(bool top_nbottom)
{
	vm::Marker marker;
	
	marker.type = vm::Marker::SPHERE;
	marker.scale.x = max_sel_.x - min_sel_.x;
	marker.scale.y = max_sel_.x - min_sel_.x;
	marker.scale.z = max_sel_.x - min_sel_.x;
	marker.pose.position.x = 0.5 * ( max_sel_.x + min_sel_.x );
	marker.pose.position.y = 0.5 * ( max_sel_.y + min_sel_.y );
	marker.pose.position.z = top_nbottom ? max_sel_.z : min_sel_.z;
	
	marker.color.r = 0.5;
	marker.color.g = 0.5;
	marker.color.b = 0.5;
	marker.color.a = 0.9;
	
	return marker;
}

//function to update the selection box
void SelectorBox::updateBox( )
{
	  vm::InteractiveMarker msg;
	  msg.header.frame_id = "/base_link";

	  vm::InteractiveMarkerControl control;
	  control.always_visible = false;
	  control.markers.push_back( makeBox(msg, min_sel_, max_sel_) );
	  control.markers.push_back( domedEnd(true) );
	  control.markers.push_back( domedEnd(false) );
	  msg.controls.push_back( control );

	  server_->insert( msg );
}


//function to make the interactive marker handles which control the box size
void SelectorBox::makeSizeHandles( )
{
    for ( int axis=0; axis<3; axis++ )
    {
      for ( int sign=-1; sign<=1; sign+=2 )
      {
        vm::InteractiveMarker int_marker;
        int_marker.header.frame_id = "/base_link";
        int_marker.scale = 1.0;

        vm::InteractiveMarkerControl control;
        control.interaction_mode = vm::InteractiveMarkerControl::MOVE_AXIS;
        control.orientation_mode = vm::InteractiveMarkerControl::INHERIT;
        control.always_visible = false;

        control.orientation.w = 1;

        switch ( axis )
        {
        case 0:
          int_marker.name = sign>0 ? "max_x" : "min_x";
          int_marker.pose.position.x = sign>0 ? max_sel_.x : min_sel_.x;
          int_marker.pose.position.y = 0.5 * ( max_sel_.y + min_sel_.y );
          int_marker.pose.position.z = 0.5 * ( max_sel_.z + min_sel_.z );
          control.orientation.x = 1;
          control.orientation.y = 0;
          control.orientation.z = 0;
          break;
        case 1:
          int_marker.name = sign>0 ? "max_y" : "min_y";
          int_marker.pose.position.x = 0.5 * ( max_sel_.x + min_sel_.x );
          int_marker.pose.position.y = sign>0 ? max_sel_.y : min_sel_.y;
          int_marker.pose.position.z = 0.5 * ( max_sel_.z + min_sel_.z );
          control.orientation.x = 0;
          control.orientation.y = 0;
          control.orientation.z = 1;
          break;
        default:
          int_marker.name = sign>0 ? "max_z" : "min_z";
          int_marker.pose.position.x = 0.5 * ( max_sel_.x + min_sel_.x );
          int_marker.pose.position.y = 0.5 * ( max_sel_.y + min_sel_.y );
          int_marker.pose.position.z = sign>0 ? max_sel_.z : min_sel_.z;
          control.orientation.x = 0;
          control.orientation.y = -1;
          control.orientation.z = 0;
          break;
        }

        interactive_markers::makeArrow( int_marker, control, 0.5 * sign );

        int_marker.controls.push_back( control );
        server_->insert( int_marker, boost::bind( &SelectorBox::processAxisFeedback, this, _1 ));
      }
    }
}


//the update function for the size handles, only updates the position as they don't need to be redrawn (as the box does) each time
void SelectorBox::updateSizeHandles( )
{
    for ( int axis=0; axis<3; axis++ )
    {
      for ( int sign=-1; sign<=1; sign+=2 )
      {
        std::string name;
        geometry_msgs::Pose pose;

        switch ( axis )
        {
        case 0:
          name = sign>0 ? "max_x" : "min_x";
          pose.position.x = sign>0 ? max_sel_.x : min_sel_.x;
          pose.position.y = 0.5 * ( max_sel_.y + min_sel_.y );
          pose.position.z = 0.5 * ( max_sel_.z + min_sel_.z );
          break;
        case 1:
          name = sign>0 ? "max_y" : "min_y";
          pose.position.x = 0.5 * ( max_sel_.x + min_sel_.x );
          pose.position.y = sign>0 ? max_sel_.y : min_sel_.y;
          pose.position.z = 0.5 * ( max_sel_.z + min_sel_.z );
          break;
        default:
          name = sign>0 ? "max_z" : "min_z";
          pose.position.x = 0.5 * ( max_sel_.x + min_sel_.x );
          pose.position.y = 0.5 * ( max_sel_.y + min_sel_.y );
          pose.position.z = sign>0 ? max_sel_.z : min_sel_.z;
          break;
        }

        server_->setPose( name, pose );
      }
    }
}


SelectorBox* selector;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "create_selection_box");
  
  // create an interactive marker server on the topic namespace simple_marker
  boost::shared_ptr<interactive_markers::InteractiveMarkerServer> server( new interactive_markers::InteractiveMarkerServer("virtual_selection") );
  
  //create the selector box class, which does everything
  selector = new SelectorBox( server );
  
  // 'commit' changes and send to all clients
  server->applyChanges();
  
  // start the ROS main loop
  ros::spin();
}
