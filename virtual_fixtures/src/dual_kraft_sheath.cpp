#include <string>
#include <vector>
#include <ros/ros.h>

#include "std_msgs/Float32.h"
#include "geometry_msgs/Point.h"
#include "sensor_msgs/JointState.h"

#include "omni_msgs/OmniFeedback.h"

#include "alex_technique/Capsule.h"
#include "alex_technique/CollisionSheath.h"

#include <visualization_msgs/Marker.h>
#include "visualization_msgs/MarkerArray.h"

#include "virtual_fixtures/Capsule.h" // msg type

Capsule capsule;
ros::Publisher feedback_pub;
float stiffness = 0;

void capsuleCallback(const virtual_fixtures::Capsule::ConstPtr& msg)
{
	Point start(msg->start.x,msg->start.y,msg->start.z);
	Point end(msg->end.x,msg->end.y,msg->end.z);
	Capsule new_capsule(start, end, msg->radius.data);
	capsule = new_capsule;
	//~ Point s = capsule.getStart();
	//~ Point e = capsule.getEnd();
	//~ ROS_INFO_STREAM("!!\t\t\t (" << s.x << " " << s.y << " " << s.z << ") [ "<< e.x << " " << e.y << " " << e.z << "] {" << capsule.getRadius() << "}");
}

void pointCallback(const geometry_msgs::Point::ConstPtr& msg)
{	
	//~ Point s = capsule.getStart();
	//~ Point e = capsule.getEnd();
	//~ ROS_INFO_STREAM("!!\t\t\t (" << s.x << " " << s.y << " " << s.z << ") [ "<< e.x << " " << e.y << " " << e.z << "] {" << capsule.getRadius() << "}");
	
	Point pt(0-(msg->x/1000.)-0.5, 0-(msg->y/1000.), (msg->z/1000.)+0.5);
	
	Vector normal;
	float penetration;
	Point closest_pt;
	bool in = capsule.PointInCapsule(pt, &normal, &closest_pt, &penetration);
	
	std::cout << "Closest point: (" << closest_pt.x << "," << closest_pt.y << "," << closest_pt.z << ")" << std::endl;
	if (in)
		std::cout << "\t Vect: (" << normal.x << "," << normal.y << "," << normal.z << ") [" << penetration << "]" << std::endl;
	omni_msgs::OmniFeedback feedback;
	if (in)
	{
	    ROS_INFO_STREAM("Inside ");// << pt.x << " " << pt.y << " " << pt.z);

	    feedback.force.x = stiffness*penetration*normal.x;
	    feedback.force.y = stiffness*penetration*normal.y+0.142;
	    feedback.force.z = stiffness*penetration*normal.z;
	}
	else
	{
        //simple gravity compensation forces
	    feedback.force.x = 0.008;
	    feedback.force.y = 0.142;
	    feedback.force.z = 0.027;			
		ROS_INFO_STREAM("--     ");// << pt.x << " " << pt.y << " " << pt.z);
	}
	feedback_pub.publish(feedback);

}

std::vector<float> kraft1angles;
std::vector<float> kraft2angles; 

void jointCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
	for (int i = 0; i < msg->name.size(); i++)
	{
		//get which joint it is from its name
		int jointidx = -1;
		float offset = 0;
		std::string name = msg->name[i];
		if (name.find("SA") != std::string::npos){
			jointidx = 0;
		}
		if (name.find("SE") != std::string::npos){
			jointidx = 1;
			offset = M_PI_2;
		}
		if (name.find("linkage_tr") != std::string::npos){
			jointidx = 2;
			offset = -M_PI_2;
		}
		if (name.find("WP") != std::string::npos){
			jointidx = 3;
		}
		if (name.find("WY") != std::string::npos){
			jointidx = 4;
		}
		if (name.find("WR") != std::string::npos){
			jointidx = 5;
		}
		
		//get which robot it is and update
		//(note +1 to the index, this is because the base link is
		//at position 0 in the kraft. maybe will remove though)
		if (jointidx != -1)
		{
			if (name.find("left") != std::string::npos)
			{
				kraft1angles[jointidx] = msg->position[i]+offset;
				//~ ROS_INFO_STREAM("Updating posn:" << msg->position[i]);
				//~ ROS_INFO_STREAM("Updating: Left [" << jointidx << "] (" << msg->name[i] << ")");
			}
			if (name.find("right") != std::string::npos)
			{
				kraft2angles[jointidx] = msg->position[i]+offset;
				//~ ROS_INFO_STREAM("Updating: Left [" << jointidx << "] (" << msg->name[i] << ")");
			}
		}
		
	}
	//~ ROS_INFO("Joint Positions Updated");
}

void stiffness_callback(const std_msgs::Float32ConstPtr& new_stiffness)
{
	stiffness = new_stiffness->data;
	ROS_INFO("New Stiffness: %f", stiffness);
}  

visualization_msgs::Marker createArrowMarker(Point point1, Point point2, int id, const std::string& base_frame, const std::string& ns, char colour, float alpha)
{
	visualization_msgs::Marker marker;	
	marker.header.frame_id = base_frame.c_str();
	marker.header.stamp = ros::Time();
	marker.ns = ns.c_str();
	marker.id = id;
	marker.type = visualization_msgs::Marker::ARROW;
	marker.action = visualization_msgs::Marker::ADD;	
	
	geometry_msgs::Point pt1;
	pt1.x = point1.x;
	pt1.y = point1.y;
	pt1.z = point1.z;
	marker.points.push_back(pt1);	
	geometry_msgs::Point pt2;
	pt2.x = point2.x;
	pt2.y = point2.y;
	pt2.z = point2.z;
	marker.points.push_back(pt2);
	
	marker.scale.x = 0.01;
	marker.scale.y = 0.01;
	
	marker.color.a = alpha;
	marker.color.r = 0.0;
	marker.color.g = 0.0;
	marker.color.b = 0.0;
	switch(colour)
	{
		case 'r': 	marker.color.r = 1.0;
					break;
		case 'g':	marker.color.g = 1.0;
					break;
		case 'b':	marker.color.b = 1.0;
					break;
		default: 	break;
	}
	
	return marker;
}

int main(int argc, char **argv)
{    
	//initialise node
  	ros::init(argc, argv, "test_classes");
  	ros::NodeHandle n;
	
	//~ Point start(1,4,-10);
	//~ Point end(1,4,10);
	//~ Capsule new_capsule(start, end, 5);
	//~ capsule = new_capsule;

	//~ KraftCollisionSheath collisions1(Point(2,0,0.801),0,0,M_PI_2);
	//~ KraftCollisionSheath collisions2(Point(3,0,0.801),0,0,M_PI_2);
	KraftCollisionSheath collisions1(Pose(Point(0,0,0.801),0,0,M_PI_2));
	KraftCollisionSheath collisions2(Pose(Point(1,0,0.801),0,0,M_PI_2));

	kraft1angles.resize(6,0);//fill with zeros
	kraft2angles.resize(6,0);

	ros::Publisher vis_pub = n.advertise<visualization_msgs::MarkerArray>( "visualization_markers", 0 );
	
	ros::Subscriber sub = n.subscribe("capsule", 10, capsuleCallback);
	ros::Subscriber sub2 = n.subscribe("ee_posn", 10, pointCallback);
	ros::Subscriber sub3 = n.subscribe("joint_states", 10, jointCallback);
	ros::Subscriber velocity_sub = n.subscribe("stiffness", 100, stiffness_callback);
	feedback_pub = n.advertise<omni_msgs::OmniFeedback>("omni1_force_feedback", 1);
	
	ros::Rate loop_rate(10);
	while (ros::ok())
	{
		//~ std::ostringstream out;
		//~ for (int i = 0; i < collisions1.nJoints(); i++)
		//~ {
			//~ Point jointposn = collisions1.getJointPosition(i);
			//~ out << "[" << jointposn.x << "," <<jointposn.y;
			//~ out << "," << jointposn.z << "]";
		//~ }
		//~ ROS_INFO_STREAM(out.str());
			
		collisions1.updateJointAngles(kraft1angles);
		collisions2.updateJointAngles(kraft2angles);
		vis_pub.publish(collisions1.createRVizMarkers(0,"world","kraft1",'r', 0.5, true)); 
		vis_pub.publish(collisions2.createRVizMarkers(1,"world","kraft2",'g', 0.5, true)); 
		
		int link1, link2;
		float dist1, dist2;
		Point point1, point2;
		if (collisions1.detectCollision(collisions2, &link1, &link2, &dist1, &dist2, &point1, &point2))
		{
			ROS_INFO_STREAM("Collision Detected. Link_left: " << link1 << " Link_right: " << link2);
			//~ ROS_INFO_STREAM("                             :{" << dist1 << "}          :{" << dist2 << "}");
			float penetration_depth = ((collisions1.getRadius() + collisions2.getRadius()) - point1.distanceTo(point2))/collisions1.getRadius() * 100.;
			ROS_INFO_STREAM("Penetration Depth = " << penetration_depth);
			
			visualization_msgs::MarkerArray markers;
			markers.markers.push_back(createArrowMarker(point1,point2, 201, "world", "arrow", 'b', 1));
			vis_pub.publish(markers);
		}
		else
		{
			visualization_msgs::MarkerArray markers;
			markers.markers.push_back(createArrowMarker(Point(),Point(), 201, "world", "arrow", 'b', 1));
			vis_pub.publish(markers);
			ROS_INFO("No Collision");
		}
			
		//~ std::cout << "Joint Posns" << std::endl;
		//~ for (int i = 0; i < collisions1.nJoints(); i++)
		//~ {
			//~ Point pt = collisions1.getJointPosition(i);
			//~ std::cout << i << ": [" << pt.x << "," << pt.y << "," << pt.z << "]" << std::endl;
		//~ }
		
		loop_rate.sleep();
		ros::spinOnce();
	}
	
	//~ std::cout << "Joint Posns" << std::endl;
	//~ for (int i = 0; i < collisions1.nJoints(); i++)
	//~ {
		//~ Point pt = collisions1.getJointPosition(i);
		//~ std::cout << i << ": [" << pt.x << "," << pt.y << "," << pt.z << "]" << std::endl;
	//~ }
	//~ std::cout << "Done" << std::endl;
	
	ros::spin();
	return 0;
}
