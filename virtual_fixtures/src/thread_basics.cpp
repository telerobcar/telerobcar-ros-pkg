#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib> // for rand

#include <boost/thread.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/date_time.hpp>

#include <ros/ros.h>
#include "sensor_msgs/JointState.h"

class UDPListeners 
{
    public:
	UDPListeners(int num_listeners, std::vector<int> ports)
	{	    	    
	    n_listeners = num_listeners;    
	    
	    for (int i = 0; i < n_listeners; i++)
	    {
		//Create a JointStates to share with each thread
		//~ boost::shared_ptr<boost::mutex> mp(new boost::mutex());
		//~ mutexes.push_back(mp);
		mutexes.push_back(new boost::mutex());
		//~ JointStateMutex empty_jointstate; //this must be address (&) because ptr_vector returns a reference (i.e. the mutex itself) 
		//~ boost::shared_ptr<int> jsp(new int(-666));
		//~ joint_states.push_back(jsp);
		joint_states.push_back(new int(-666));
		std::cout << "Creating Thread " << i << std::endl;
		//~ listener_threads.create_thread(boost::bind(&UDPListeners::listen_function, this, ports[i], joint_states[i], mutexes[i]));
		listener_threads.create_thread(boost::bind(&UDPListeners::listen_function, this, ports[i], boost::ref(joint_states[i]), boost::ref(mutexes[i])));
	    }
	}
	
	void handle_stop()
	{
	    //ask all threads to close
	    listener_threads.interrupt_all();	
	    
	    std::cout << "threads interrupted" << std::endl;
	    
	    //wait until they actually do
	    listener_threads.join_all();
	}
	
	//~ void listen_function(int port_num, JointStateMutex& joints)
	//~ void listen_function(int port_num, boost::shared_ptr<int> jointstateptr, boost::shared_ptr<boost::mutex> mutexptr)
	void listen_function(int port_num, int& jointstateptr, boost::mutex& mutexptr)
	{
	    for (;;)
	    {		
		boost::posix_time::seconds workTime(rand()%10);
		//~ sensor_msgs::JointState new_joint_state;
		//~ std::ostringstream name;
		//~ name << "Thread " << port_num << ": id: ";
		//~ for (int j = 0; j < rand()%5+1; j++)
		//~ {
		    //~ std::ostringstream joint;
		    //~ joint << j;
		    //~ new_joint_state.name.push_back(name.str()+joint.str());
		    //~ new_joint_state.position.push_back((float)(rand()%100));
		//~ }
		int randnumber = rand()%10;
		std::cout << "Writer " << port_num << " -> " << randnumber << std::endl;
		//~ joints.write(randnumber);
		//~ (*mutexptr).lock();
		//~ *jointstateptr = randnumber;
		//~ (*mutexptr).unlock();
		mutexptr.lock();
		jointstateptr = randnumber;
		mutexptr.unlock();
		std::cout << "Writer " << port_num << " writing (" << randnumber << ") Succeeded" << std::endl;
		std::cout << std::endl;
		
		boost::this_thread::sleep(workTime);
	    }
	    
	    std::cout << "Port Number: " << port_num << " finished" << std::endl;
	}
	
	void print_states()
	{
	    std::cout << std::endl;
	    std::cout << "print_states called" << std::endl;
	    boost::posix_time::seconds workTime(rand()%10);
	    std::cout << "Nlisteners = " << n_listeners << std::endl;
	    for (int i = 0; i < n_listeners; i++)
	    {
		//~ sensor_msgs::JointState read_state;
		int read_state;
		std::cout << "Reader " << i << " reading" << std::endl;
		//~ read_state = joint_states[i].read();
		//~ (*mutexes[i]).lock();
		//~ read_state = *joint_states[i];
		//~ (*mutexes[i]).unlock();
		mutexes[i].lock();
		read_state = joint_states[i];
		mutexes[i].unlock();
		std::cout << "Reader " << i << " <- " << read_state << std::endl;
		//~ int njoints = (int)read_state.name.size();
		//~ std::cout << "[";
		//~ for (int j = 0; j < njoints; j++)
		//~ {
		    //~ std::cout << read_state.name[j] << ",";
		//~ }
		//~ std::cout << "]" << std::endl;
		//~ std::cout << "(";
		//~ for (int j = 0; j < njoints; j++)
		//~ {
		    //~ std::cout << read_state.position[j] << ",";
		//~ }
		//~ std::cout << ")" << std::endl;
		//~ std::cout << std::endl;
	    }
	    boost::this_thread::sleep(workTime);
	}
	
	int n_listeners;
	
    private:
	boost::thread_group listener_threads;  
	//~ std::vector< boost::shared_ptr<sensor_msgs::JointState> > joint_states;
	//~ std::vector< boost::shared_ptr<boost::mutex> > mutexes;
	boost::ptr_vector<int> joint_states;
	boost::ptr_vector<boost::mutex> mutexes;
};


int main(int argc, char* argv[])
{
    std::cout << "main: startup" << std::endl;
    
    int ports[] = {0, 1, 2, 3, 4};
    //~ int ports[] = {5551, 5552, 5553, 5554};
    std::vector<int> portsvect(ports, ports+sizeof(ports)/sizeof(int));
    UDPListeners listeners(5, portsvect);
    for (;;)
    {
	listeners.print_states();
	//wait until ctrl-c called
    }
    
    std::cout << "calling stop" << std::endl;
    
    listeners.handle_stop();
    
    std::cout << "main: done" << std::endl;
    
    
    return 0;
}
